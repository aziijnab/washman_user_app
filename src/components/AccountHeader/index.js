import React, { Component } from "react";
import { Header, Button, Left, Right, Icon, Container, Form, Textarea,Badge } from "native-base";
// import { inject } from "mobx-react/native";
// import IconBadge from "react-native-icon-badge";
import {
  Dimensions,
  Platform,
  StatusBar,
  Modal,
  Text,
  TouchableHighlight,
  View,
  Alert,
  Image,
  AsyncStorage
} from "react-native";

const { height, width } = Dimensions.get("window");

// @inject("routerActions")
class AccountHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      total_items:null
    };
    this.navigate = this.props.navigation.navigate;
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  componentWillMount() {
    this.startHeaderHeight = 100;
    if (Platform.OS == "android") {
      this.startHeaderHeight = 50 + StatusBar.currentHeight;
    }
    AsyncStorage.getItem("total_items").then(total_items_data => {
      const total_items_count = JSON.parse(total_items_data);
     
      if(total_items_count){
        this.setState({total_items : total_items_count})
      }
    });
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <View>
        <Header style={{backgroundColor:"#1c2631"}}>
          {this.props.back ? (
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon style={{ color: "#fff" }} name="arrow-back" />
                
              </Button>
            </Left>
          ) : null}
          <View style={{alignContent:"center", justifyContent:"center"}}>
          <Text
                    style={{
                      color: "#fff",
                      fontSize: 34,
                      textAlign: "center",
                      fontFamily:"neuron_regular"
                    }}
                  >
                   WASHMAN
                  </Text>
          </View>
         
          <Right>
           
            <Button
 onPress={() => this.props.navigation.navigate("Order")}
              transparent
            >
              <Icon style={{ color: "#fff" }} name="cart" />
              {this.state.total_items ? (
                <Badge style={{height:18}} primary>
            <Text style={{fontSize:14, fontFamily:"neuron_regular", color:"#fff"}}>{this.state.total_items ? this.state.total_items : 0 }</Text>
          </Badge>
              ) : null}
             
            </Button>
            

          </Right>
        </Header>

        <Modal
          animationType={"fade"}
          transparent={true}
          onRequestClose={() => this.setModalVisible(false)}
          visible={this.state.modalVisible}
        >
          <View style={styles.popupOverlay}>
            <View style={styles.popup}>
              <View style={styles.popupContent}>
              <TouchableHighlight  onPress = {() => {this.setState({modalVisible:false})}}>
<View style={{marginLeft:"auto",alignContent:"center", alignItems:"center", justifyContent:"center", borderWidth:1, borderRadius:10, width:20, height:20, backgroundColor:"#000"}}> 
<Icon style={{color:"#fff"}} name="ios-close"></Icon>
</View>
</TouchableHighlight>
                <Text
                  style={{
                    color: "#000",
                    fontSize: 18,
                    textAlign: "center",
                    marginVertical: 5
                  }}
                >
                  Ask a Question
                </Text>
              
                <View style={styles.userInfo}>
                  <View style={{ flexDirection: "row", width: "100%" }}>
                  <View style={{marginTop:10, paddingVertical:20}}>

                    <Image
                      source={{
                        uri:
                          "https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"
                      }}
                      style={styles.profileImg}
                    />
                  </View>
                  <View style={{padding:20, marginTop:10}}>

                    <Text style={{fontSize:18, color:"#000"}}>John Doe</Text>
                    <Text style={{fontSize:14, color:"#000"}}>@johndoe</Text>

                  </View>
                  </View>
                  <View style={{width: "100%" }}>
                  <Form>
            <Textarea rowSpan={5} bordered placeholder="What do you want to ask ?" />
          </Form>
                  </View>
                  <View style={{ flexDirection: "row", width: "100%" }}>
                  
                  <View style={{marginTop:10, paddingVertical:20, width:120, marginRight:"auto", marginLeft:10}}>

                 <Button bordered dark style={{width:"90%"}}>
                 <View style={{width:"100%", alignItems:"center", alignContent:"center"}}>

                 <Text style={{textAlign:"center"}}>Add Image</Text> 
                 </View>
                 </Button>
                  </View>
                  <View style={{marginTop:10, paddingVertical:20, width:80, marginLeft:"auto", marginRight:10}}>

                 <Button bordered dark style={{width:"90%"}}>
                 <View style={{width:"100%", alignItems:"center", alignContent:"center"}}>

                 <Text style={{textAlign:"center"}}>Post</Text> 
                 </View>
                 </Button>
                  </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = {
  popup: {
    backgroundColor: "white",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    backgroundColor: "#00000057",
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    margin: 5,
  },
  popupHeader: {
    marginBottom: 45
  },
  userInfo: {
    width: "100%"
  },
  profileImg: {
    height: 50,
    width: 50,
    borderRadius: 25
  },
  socialImg: {
    height: 45,
    width: 45,
    borderRadius: 23
  },
};

export default AccountHeader;
