import React from "react";
import {
  createStackNavigator,
  createAppContainer,
  StackNavigator
} from "react-navigation";
import UpdateAccount from "./screens/LoginScreens/UserProfile/UpdateAccount";
import LoginForm from "./screens/LoginScreens/LoginForm";
import RegisterScreens from "./screens/RegisterScreens";
import HomeTab from "./screens/LoginScreens/UserProfile/home_tabs/HomeTab";
import ActionPage from "./screens/LoginScreens/UserProfile/home_tabs/ActionPage";
import PackageSubscription from "./screens/LoginScreens/UserProfile/home_tabs/PackageSubscription";
import AvailablePromotions from "./screens/LoginScreens/UserProfile/home_tabs/AvailablePromotions";
import Quantity from "./screens/LoginScreens/UserProfile/home_tabs/Quantity";
import Order from "./screens/LoginScreens/UserProfile/home_tabs/Order";
import PromotionOrder from "./screens/LoginScreens/UserProfile/home_tabs/PromotionOrder";
import SubcriptionOrder from "./screens/LoginScreens/UserProfile/home_tabs/SubcriptionOrder";
import OrderFromPackage from "./screens/LoginScreens/UserProfile/home_tabs/OrderFromPackage";
import CurrentPackage from "./screens/LoginScreens/UserProfile/home_tabs/CurrentPackage";
import CategoryDetails from "./screens/LoginScreens/UserProfile/home_tabs/CategoryDetails";
import TermsandConditions from "./screens/LoginScreens/UserProfile/home_tabs/TermsandConditions";
import RecoverPassword from "./screens/LoginScreens/RecoverPassword";
import CustomerSupport from "./screens/LoginScreens/CustomerSupport";
import Settings from "./screens/LoginScreens/UserProfile/Settings";
import OrderHistory from "./screens/LoginScreens/UserProfile/OrderHistory";
import DeactivateAccount from "./screens/LoginScreens/UserProfile/DeactivateAccount";
import ForgotPassword from "./screens/LoginScreens/ForgotPassword";

const TopLevelLoginNavigator = createStackNavigator(
  {
    ActionPage: { screen: ActionPage },
    HomeTab: { screen: HomeTab },
    CategoryDetails: { screen: CategoryDetails },
    TermsandConditions: { screen: TermsandConditions },
    Quantity: { screen: Quantity },
    CurrentPackage:{screen:CurrentPackage},
    PackageSubscription: { screen: PackageSubscription },
    AvailablePromotions: { screen: AvailablePromotions },
    OrderHistory: { screen: OrderHistory },
    Order: { screen: Order},
    PromotionOrder: { screen: PromotionOrder},
    SubcriptionOrder: { screen: SubcriptionOrder},
    OrderFromPackage: { screen: OrderFromPackage},
    UpdateAccount: { screen: UpdateAccount },
    DeactivateAccount: { screen: DeactivateAccount },
    CustomerSupport: { screen: CustomerSupport },
    ForgotPassword: { screen: ForgotPassword },
    RecoverPassword: { screen: RecoverPassword },
    Settings: { screen: Settings },
    LoginForm: { screen: LoginForm },
    RegisterScreens: { screen: RegisterScreens },
  },
  {
    headerMode: "none"
  }
  );

  const TopLevelLogin = createAppContainer(TopLevelLoginNavigator);
export default TopLevelLogin;
