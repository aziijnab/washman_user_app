//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator,
  BackHandler
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import { Button, Container } from "native-base";
import BaseHeader from "../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      username_err: "",
      password_err: "",
      isLoading: "",
      userInfo: null,
      DeviceID: "",
    };
  }
  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        console.log("not granted")
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
      }
      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        console.log("not granted")
      }
      let token = await Notifications.getExpoPushTokenAsync();
      console.log(token)
      this.setState({ DeviceID : token})
  }
  
 componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    
  }
  handleBackButton = () => {
    Alert.alert(
        'Exit App',
        'Exiting the application?', [{
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
        }, {
            text: 'OK',
            onPress: () => BackHandler.exitApp()
        }, ], {
            cancelable: false
        }
     )
     return true;
   } 

  login_form = async (username,password) => {
    this.setState({ isLoading: true });
    console.log("id = " + this.state.DeviceID);
      fetch(URL + "Customer/Singin", {
        method: "GET",
        headers: {
         AuthToken:TOKEN,
         Username: username,
          Password: password,
          DeviceID: this.state.DeviceID,
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.ResponseCode == 200) {
            AsyncStorage.setItem("userID", JSON.stringify(response.list[0]));
            this.setState({ isLoading: false });
            this.props.navigation.push("ActionPage")
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }


        })
        .catch(error => alert("Please check internet connection"));
  }
  submit = () => {
    const { username, password } = this.state;
    let username_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (username == "" && password == "") {
      this.setState({
        username_err: "username should not be empty",
        password_err: "Password should not be empty"
      });
    }
    
    if (username == "") {
      this.setState({ username_err: "Required" });
    } else {
      this.setState({
        username_err: ""
      });

    }
    if (password == "") {
      this.setState({ password_err: "Required" });
    } else {
      this.setState({
        password_err: ""
      });

    }
    if(username != "" && password != ""){

      this.login_form(username,password);
    }
  };
  render() {
    const { navigation } = this.props.navigation;
    const { username, password } = this.state;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
<Image style={{height:90, width:deviceWidth, resizeMode:"cover"}} source={require("../../../../assets/header.png")}/>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}>
               Login
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100
              }}
            >
              <TextField
                autoCapitalize="none"
                error={this.state.username_err}
                onChangeText={username => this.setState({ username })}
                autoCorrect={false}
                label="Username"
                maxLength={40}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
              <TextField
                secureTextEntry={true}
                error={this.state.password_err}
                autoCapitalize="none"
                onChangeText={password => this.setState({ password })}
                autoCorrect={false}
                label="Password"
                maxLength={40}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                  onPress={this.submit}
                 
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Login
                  </Text>
                </Button>
              )}
              
                
              </View>
             
              <View style={{ paddingVertical: 20, flexDirection: "row" }}>
              <View style={{ width: "70%" }}>
                <TouchableOpacity 
      onPress={() => this.props.navigation.navigate("ForgotPassword")}
      >
      <Text
                    style={{
                      color: "#000",
                      fontSize: 24,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Forgot Password?
                  </Text>
        </TouchableOpacity>
                  
                </View>
              
                <View style={{ width: "30%" }}>
                <TouchableOpacity 
      onPress={() => this.props.navigation.navigate("RegisterScreens")}>
     <Text
                    style={{
                      color: "#000",
                      fontSize: 24,
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Sign Up
                  </Text>
        </TouchableOpacity>
                 
                </View>
               </View>
              </View>
             
            {/* ............Button................. */}
          </View>
          <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", marginTop:"auto", marginBottom:50}}>
         <Text style={{fontFamily:"neuron_regular", fontSize:12, color:"#808080"}}>Version : 1.0.2</Text>
       </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default LoginForm;
