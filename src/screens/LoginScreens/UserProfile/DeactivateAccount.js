//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Container } from "native-base";
import AccountHeader from "../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";
import { URL , TOKEN } from "../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class DeactivateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: "",
    };
  }
submit = () => {
  this.setState({ isLoading: true });
      fetch(URL + "Customer/DeActivateCustomer", {
        method: "PUT",
        headers: {
         AuthToken:TOKEN,
         CustomerID: 222,
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.ResponseCode == 200) {
            this.setState({ isLoading: false });
            this.props.navigation.push("ActionPage")
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }


        })
        .catch(error => alert("Please Check Your Internet Connection"));
}
  render() {
    const { navigation } = this.props.navigation;
    const { email, password } = this.state;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
       <AccountHeader navigation={this.props.navigation} back={true} />
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:50}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_regular",textAlign:'center' }}>
               DEACTIVATE ACCOUNT
              </Text>
             </View>
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingTop:"20%", paddingHorizontal:70}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_regular",textAlign:'center' }}>
               CLICK THE BUTTON TO DEACTIVATE YOUR ACCOUNT
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100,
                paddingTop:'10%'
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 30,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                  onPress={this.submit}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 30,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Deactivate Account
                  </Text>
                </Button>
              )}
              </View>
              </View>

            {/* ............Button................. */}
          </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default DeactivateAccount;
