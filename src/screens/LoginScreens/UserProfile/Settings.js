//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  Image,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import Constants from 'expo-constants'
import { Button, Content, Container } from "native-base";
import AccountHeader from "../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Settings extends Component {
  constructor(props) {
    super(props);
    this.navigate = this.props.navigation.navigate;
  }
  logout = () => {
    AsyncStorage.clear();
    this.props.navigation.push("LoginForm");
  }
  render() {
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
     
     <AccountHeader
            back={true}
            navigation={this.props.navigation}
         />
          <View
            style={{
              alignItems: "center",
              paddingTop: "5%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text   style={{
                      color: "#000",
                      fontSize: 34,
                      textAlign: "center",
                      fontFamily:"neuron_bold"
                    }}>
               Settings
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
                bordered
                onPress={() => {
                this.props.navigation.push("UpdateAccount");
              }}
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 50,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 25,
                    fontFamily:"neuron_regular"
                  }}
                >
                  Edit Profile
                </Text>
              </Button>
            </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingTop: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
               onPress={() => this.props.navigation.navigate("RecoverPassword")}
                bordered
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 25,
                    fontFamily:"neuron_regular"
                  }}
                >
                  Change Password
                </Text>
              </Button>
            </View>

            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingTop: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
              onPress={() => this.props.navigation.navigate("CustomerSupport")}
                bordered
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 25,
                    fontFamily:"neuron_regular"
                  }}
                >
                  Customer Support
                </Text>
              </Button>
            </View>
            
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingTop: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
              onPress={() => this.props.navigation.navigate("DeactivateAccount")}
                bordered
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 25,
                    fontFamily:"neuron_regular"
                  }}
                >
                  Deactivate Account
                </Text>
              </Button>
            </View>
            
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingTop: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
              onPress={this.logout}
                bordered
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 25,
                    fontFamily:"neuron_regular"
                  }}
                >
                  Logout
                </Text>
              </Button>
            </View>
            {/* ............Button................. */}
          </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default Settings;
