//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import Constants from "expo-constants";
import {
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Container,
  Right
} from "native-base";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
import buttonImg from "../../../../../assets/button.png";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Quantity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subcategories_item: [],
      isLoading: false,
      items: [],
      items_quantity: [],
      order: [],
      SubCategoryID: 2,
      total_price: 0,
      total_items: 0,

      final_order : []
    };
    this.navigate = this.props.navigation.navigate;
    this.data = this.props.navigation.state.params;
  }
  componentDidMount() {
    AsyncStorage.getItem("total_price").then(price_data => {
      const price_total = JSON.parse(price_data);

      if (price_total) {
        this.setState({ total_price: price_total });
      }
    });
    AsyncStorage.getItem("total_items").then(total_items_data => {
      const total_items_count = JSON.parse(total_items_data);

      if (total_items_count) {
        this.setState({ total_items: total_items_count });
      }
    });
    AsyncStorage.getItem("items").then(items_data => {
      const items_d = JSON.parse(items_data);

      if (items_d) {
        this.setState({ items: items_d });
      }
    });
    AsyncStorage.getItem("items_quantity").then(i_quantity_data => {
      const i_quantity_d = JSON.parse(i_quantity_data);

      if (i_quantity_d) {
        this.setState({ items_quantity: i_quantity_d });
      }
    });
 
    fetch(URL + "Item/GetBySubCategoryID", {
      method: "GET",
      headers: {
        AuthToken: TOKEN,
        SubCategoryID: this.data.subCategory_id
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.ResponseCode == 200) {
          var data = response.list;
          AsyncStorage.getItem("selected_objects").then(aa => {
            const aas = JSON.parse(aa);
            if(aas){
              data.forEach(Item => {
                var runtime_object = aas.find(
                  task => task.ItemID === Item.ItemID
                );
                if(runtime_object){
                  Item['quantity'] = runtime_object.quantity;
                  // console.log(Item);
                }
               
              })
            }
                 console.log(data);
                 this.setState({
                  isLoading: false,
                  subcategories_item: data
                });
          });
        
        
          // console.log(data);
         
        } else {
          Alert.alert("Sorry", response.MsgToShow, [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      // .catch(error => alert("Please Check Your Internet Connection"));
      .catch(error => alert("Please Check your Internet Connection"));
  }
  removeItem = item_id => {
    
    AsyncStorage.removeItem("items_quantity");
    AsyncStorage.removeItem("total_items");
    AsyncStorage.removeItem("total_price");

    const result = this.state.items.findIndex(task => task === item_id);
    var indexOfItem = this.state.subcategories_item.find(
      task => task.ItemID === item_id
    );
    //  this.state.order.push(indexOfItem);
    var ItemPrice = parseInt(indexOfItem.UnitPrice);
    this.setState({
      total_price: this.state.total_price - ItemPrice,
      total_items: this.state.total_items - 1
    });
    var tota_p = this.state.total_price - ItemPrice;
    var tota_q = this.state.total_items - 1;
    this.state.items_quantity[result]--;
    indexOfItem.quantity--;
    // console.log(indexOfItem.quantity)
    if (indexOfItem.quantity == 0) {
      
      this.state.items.pop(result);
      this.state.items_quantity.pop(result);
    }
        this.setState({
          subcategories_item: this.state.subcategories_item
        });
   
    AsyncStorage.setItem(
      "items_quantity",
      JSON.stringify(this.state.items_quantity)
    );
    AsyncStorage.setItem("total_items", JSON.stringify(tota_q));
    AsyncStorage.setItem("total_price", JSON.stringify(tota_p));
    AsyncStorage.getItem("selected_objects").then(array_of_selections => {
      const selected_array = JSON.parse(array_of_selections);
      var check_selected_array_object = this.state.subcategories_item.find(
        task => task.ItemID === item_id
      );
      if(selected_array){
     
        // console.log(check_selected_array_object.ItemID)
        var check_selected_obj_id_present = selected_array.find(
          task => task.ItemID === check_selected_array_object.ItemID
        );
        // console.log("present or not :")
        // console.log(check_selected_obj_id_present);
        if (check_selected_obj_id_present) {
          
          check_selected_obj_id_present.quantity--;
          console.log(selected_array)
          // console.log(check_selected_array_object)
          AsyncStorage.setItem(
            "selected_objects",
            JSON.stringify(selected_array)
          );
        } else{
          selected_array.push(check_selected_array_object)
          AsyncStorage.setItem(
            "selected_objects",
            JSON.stringify(selected_array)
          );
          console.log(selected_array)
        }
      }else{
        var obj_arr = [];
        obj_arr.push(check_selected_array_object)
        AsyncStorage.setItem(
          "selected_objects",
          JSON.stringify(obj_arr)
        );
      }

    });
  };
  addItem = item_id => {
    const result = this.state.items.findIndex(task => task === item_id);
    var indexOfItem = this.state.subcategories_item.find(
      task => task.ItemID === item_id
    );
    this.state.order.push(indexOfItem);
    var ItemPrice = parseInt(indexOfItem.UnitPrice);
    this.setState({
      total_price: this.state.total_price + ItemPrice,
      total_items: this.state.total_items + 1
    });
    var tot_p = this.state.total_price + ItemPrice;
    var tot_q = this.state.total_items + 1;
    AsyncStorage.setItem("total_price", JSON.stringify(tot_p));
    AsyncStorage.setItem("total_items", JSON.stringify(tot_q));
   
    if (result < 0) {
      this.state.items.push(item_id);
      this.state.items_quantity.push(1);
      indexOfItem["quantity"] = 1;
      indexOfItem["service"] = this.data.subCategory_name;
     
      this.setState({
        subcategories_item: this.state.subcategories_item,
      });

    } else {
      this.state.items_quantity[result]++;
      indexOfItem.quantity++;
      this.setState({
        subcategories_item: this.state.subcategories_item
      });
      var indexOfItemFinal = this.state.final_order.find(
        task => task.ItemID === item_id
      );
      AsyncStorage.setItem(
        "items_quantity",
        JSON.stringify(this.state.items_quantity)
      );
    }
    // AsyncStorage.setItem("items_arr", JSON.stringify(this.state.items));
    AsyncStorage.getItem("items_arr").then(array_of_items => {
      const item_array = JSON.parse(array_of_items);

      if(item_array){
        const check_item_array_id = item_array.findIndex(task => task === item_id);
        // console.log("Array :" + item_array)
        // console.log("check :" + check_item_array_id)
        if (check_item_array_id < 0) {
          item_array.push(item_id);
          AsyncStorage.setItem(
            "items_arr",
            JSON.stringify(item_array)
          );
        } 
      }else{
        AsyncStorage.setItem(
          "items_arr",
          JSON.stringify(this.state.items)
        );
      }

    });
    AsyncStorage.getItem("selected_objects").then(array_of_selections => {
      const selected_array = JSON.parse(array_of_selections);
      var check_selected_array_object = this.state.subcategories_item.find(
        task => task.ItemID === item_id
      );
      if(selected_array){
     
        // console.log(check_selected_array_object.ItemID)
        var check_selected_obj_id_present = selected_array.find(
          task => task.ItemID === check_selected_array_object.ItemID
        );
        // console.log("present or not :")
        // console.log(check_selected_obj_id_present);
        if (check_selected_obj_id_present) {
          check_selected_obj_id_present.quantity++;
          console.log(selected_array)
          // console.log(check_selected_array_object)
          AsyncStorage.setItem(
            "selected_objects",
            JSON.stringify(selected_array)
          );
        } else{
          selected_array.push(check_selected_array_object)
          AsyncStorage.setItem(
            "selected_objects",
            JSON.stringify(selected_array)
          );
          console.log(selected_array)
        }
      }else{
        var obj_arr = [];
        obj_arr.push(check_selected_array_object)
        AsyncStorage.setItem(
          "selected_objects",
          JSON.stringify(obj_arr)
        );
      }

    });
  };
  render() {
    return (
      <Container>
        <AccountHeader navigation={this.props.navigation} back={true} />
        <View
          style={{
            width: deviceWidth,
            paddingTop: 20,
            paddingBottom: 5,
            alignItems: "center",
            alignContent: "center"
          }}
        >
          <Text
            style={{
              color: "#000",
              fontSize: 34,
              textAlign: "center",
              fontFamily: "neuron_bold"
            }}
          >
            {this.data.subCategory_name}
          </Text>
          {/* <Text
                    style={{
                      color: "#000",
                      fontSize: 14,
                      textAlign: "center",
                      fontFamily:"neuron_regular"
                    }}
                  >
                  We will Wash and Fold your clothes (No Ironing)
                  </Text> */}
        </View>
        <Content>
          {this.state.subcategories_item.map((item, index) => {
            return (
              <View
                key={index}
                style={{
                  width: deviceWidth,
                  alignContent: "center",
                  alignItems: "center",
                  marginVertical: 5
                }}
              >
               
                  <View
                    style={{
                      width: "90%",
                      paddingHorizontal: 10,
                      backgroundColor: "#fff",
                      borderRadius: 50,
                      borderColor: "#808080",
                      borderWidth:0.5,
                      flexDirection: "row",
                      height: 40,
                      justifyContent: "center",
                      alignItems: "center",
                      alignContent: "center"
                    }}
                  >
                  <View style={{width:"45%"}}>
                  <Text
                      style={{
                        color: "#000",
                        fontSize: 14,
                        fontFamily: "neuron_regular"
                      }}
                    >
                      {" "}
                      {item.Name}
                    </Text>
                  </View>
                    
                    <View style={{width:"40%"}}>
                    <View style={{flexDirection:"row",alignContent:"center", alignItems:"center"}}>
                    {item.quantity ? (
                      <TouchableOpacity
                        onPress={() => this.removeItem(item.ItemID)}
                      >
                    <View
                          style={{
                            width: 28,
                            borderRadius: 15,
                            alignContent: "center",
                            alignContent: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: "#000",
                              textAlign: "center",
                              fontSize: 30,
                              fontWeight: "bold"
                            }}
                          >
                            -
                          </Text>
                        </View>
                        </TouchableOpacity>
                    ) : null}
                        <View
                      style={{
                        width: 28,
                        borderRadius: 15,
                        backgroundColor: "#000",
                        alignContent: "center",
                        alignContent: "center"
                      }}
                    >
                      <Text
                        style={{
                          color: "#fff",
                          textAlign: "center",
                          fontSize: 12
                        }}
                      >
                        {item.quantity ? item.quantity : 0}
                      </Text>
                    </View>
                    <TouchableOpacity
                  onPress={() => this.addItem(item.ItemID)}
                >
                    <View>
                    <View
                          style={{
                            width: 28,
                            borderRadius: 15,
                            alignContent: "center",
                            alignContent: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: "#000",
                              textAlign: "center",
                              fontSize: 30,
                              fontWeight: "bold"
                            }}
                          >
                            +
                          </Text>
                        </View>
                    </View>
                   </TouchableOpacity>
                    
                    </View>
                    </View>
                    <View style={{width:"15%"}}>
                    <Text
                      style={{

                        color: "#000",
                        fontSize: 14,
                        fontFamily: "neuron_regular",
                        marginLeft: "auto"
                      }}
                    >
                      {" "}
                      Rs.{item.UnitPrice}
                    </Text>
                    </View>
                     
                  </View>
                
              </View>
            );
          })}
        </Content>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("Order", {
              total_items: this.state.total_items,
              total_price: this.state.total_price,
              order_data: this.state.subcategories_item,
              item_ids: this.state.items,
              item_quantities: this.state.items_quantity
            })
          }
        >
          <View
            style={{
              width: deviceWidth,
              alignContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width: "90%",
                backgroundColor: "#45c8d0",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "stretch",
                marginBottom: 10,
                paddingVertical: 10,
                paddingHorizontal: 5
              }}
            >
              <View>
                <Text style={{ color: "#fff", marginLeft: 5 }}>
                  {this.state.total_items}
                </Text>
              </View>
              <View>
                <Text style={{ color: "#fff", fontSize: 20 }}>
                  View Your Cart
                </Text>
              </View>
              <View>
                <Text style={{ color: "#fff", marginRight: 5 }}>
                  Rs. {this.state.total_price}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>

        <Footer>
          <FooterTab style={{backgroundColor:"#1c2631"}}>
            <Button vertical
            onPress={() => {this.props.navigation.push("ActionPage")}}>
              <Icon style={styles.colorWhite} name="home"/>
            </Button>
            <Button vertical 
          onPress={() => {
                this.props.navigation.push("CustomerSupport");
              }}
              >
              <Icon style={styles.colorWhite} name="mail" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("OrderHistory");
              }}
              >
              <Icon style={styles.colorWhite} active name="list-box" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("CurrentPackage");
              }}
              >
              <Icon style={styles.colorWhite} active name="cube" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("Settings");
              }}
              >
              <Icon style={styles.colorWhite} active name="settings" />
            </Button>
            
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#808080",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
  colorWhite: {
    color: "#fff"
  }
});

export default Quantity;


