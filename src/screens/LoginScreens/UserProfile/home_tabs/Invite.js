//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Container } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL } from "../../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      email_err: "",
      password_err: "",
      isLoading: "",
      userInfo: null,
    };
  }

 
  
  login = (first_name, last_name, email,location) => {
    this.setState({ isLoading: true });
      fetch(URL + "fb-login", {
        method: "POST",
        body: JSON.stringify({
          first_name: first_name,
          last_name: last_name,
          email: email,
          location: location
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(async response => {
          if (response.response == "success") {
            this.setState({ isLoading: false });
           this.props.navigation.navigate('UserProfile')
          } 
           else {
            Alert.alert(
              'Sorry',
              response.message,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }
        })
        .catch(error => alert("Please Check Your Internet Connection"));
  }
  login_form = (email,password) => {
    this.setState({ isLoading: true });
      fetch(URL + "sign-in", {
        method: "POST",
        body: JSON.stringify({
         
          email: email,
          password: password
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(async response => {
          if (response.response == "success") {
            this.setState({ isLoading: false });
           this.props.navigation.navigate('UserProfile')
          } 
           else {
            Alert.alert(
              'Sorry',
              response.message,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }


        })
        .catch(error => alert("Please Check Your Internet Connection"));
  }
  submit = () => {
    const { email, password } = this.state;
    let email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email == "" && password == "") {
      this.setState({
        email_err: "Email should not be empty",
        password_err: "Password should not be empty"
      });
    }
    if (email != "") {
      if (email_reg.test(email) === false) {
        this.setState({ email_err: "Invalid Email" });
      } else {
        this.setState({ email_err: "" });
      }
    }
    if (password == "") {
      this.setState({ password_err: "Required" });
    } else {
      this.setState({
        password_err: ""
      });

    }
    if(email != "" && password != ""){
      this.login_form(email,password);
    }
  };
  render() {
    const { navigation } = this.props.navigation;
    const { email, password } = this.state;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
<Image style={{height:90, width:deviceWidth, resizeMode:"cover"}} source={require("../../../../../assets/header.png")}/>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:50}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_regular",textAlign:'center' }}>
               "Invite your Friends and tell them about us"
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100,
                paddingTop:'10%'
              }}
            >
              <TextField
              
                autoCapitalize="none"
                error={this.state.email_err}
                keyboardType={"email-address"}
                onChangeText={email => this.setState({ email })}
                autoCorrect={false}
                label="Email"
                maxLength={40}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
             
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: deviceHeight/5,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                  // onPress={this.submit}
                  onPress={() => this.props.navigation.navigate("HomeTab")}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: deviceHeight/5,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    SEND
                  </Text>
                </Button>
              )}
              </View>
              </View>

            {/* ............Button................. */}
          </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default Invite;
