//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Image,
  TouchableOpacity,
} from "react-native";
import Constants from "expo-constants";
import {
  Content,
  Text,
  Button,
  Icon,
  Footer, FooterTab,
  Container
} from "native-base";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
import buttonImg from "../../../../../assets/button.png";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class HomeTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      categories_data:[]
    };
    this.navigate = this.props.navigation.navigate;
  }
 componentWillMount(){
  fetch(URL + "Category/Get", {
    method: "GET",
    headers: {
     AuthToken:TOKEN,
    }
  })
    .then(res => res.json())
    .then(async response => {
      console.log(response);
      if (response.ResponseCode == 200) {
        this.setState({ isLoading: false, categories_data : response.list });
      } 
       else {
        Alert.alert(
          'Sorry',
          response.MsgToShow,
          [
            {text: 'OK'},
          ],
          {cancelable: true},
        );
        this.setState({ isLoading: false });
      }

    })
    .catch(error => alert("Please Check Your Internet Connection"));
 }
  render() {
    return (
        <Container>
       <AccountHeader navigation={this.props.navigation} back={true} />
       <View
            style={{
              width: deviceWidth,
              paddingTop: 20,
              paddingBottom: 20,
              alignItems: "center",
              alignContent: "center"
            }}
          >
         
           <Image style={{height:200, width:"90%", resizeMode:"cover", paddingHorizontal:50}} source={require("../../../../../assets/header_white.png")}/>

          </View>
          <Content>
          { 
                    this.state.categories_data.map((item, index)=>{
                        return (
                          <View key={index} style={{width:deviceWidth, alignContent:"center", alignItems:"center",marginVertical:10}}>
          <View style={{width:"90%"}}>
          <TouchableOpacity
          onPress={() => this.props.navigation.navigate("CategoryDetails", 
          {
              category_id: item.CategoryID,
              category_name: item.Name,
            }
          )}>
          <ImageBackground style={{width:"100%", height:60, alignItems: "flex-start",justifyContent: "center",paddingLeft:20}} source={buttonImg}>
          <Text style={{ fontSize: 30, color: "#fff", fontFamily:"neuron_bold" }}>
               {item.Name}
              </Text>
          </ImageBackground>
          </TouchableOpacity>
          </View>
          </View>
                        )
                    })
                }
         {/* ............. */}
          </Content>
          <Footer>
          <FooterTab style={{backgroundColor:"#1c2631"}}>
            <Button vertical
             onPress={() => {
                this.props.navigation.push("ActionPage");
              }}>
              <Icon style={styles.colorWhite} name="home" />
            </Button>
            <Button vertical 
          onPress={() => {
                this.props.navigation.push("CustomerSupport");
              }}
              >
              <Icon style={styles.colorWhite} name="mail" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("OrderHistory");
              }}
              >
              <Icon style={styles.colorWhite} active name="list-box" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("CurrentPackage");
              }}
              >
              <Icon style={styles.colorWhite} active name="cube" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("Settings");
              }}
              >
              <Icon style={styles.colorWhite} active name="settings" />
            </Button>
          </FooterTab>
        </Footer>

      </Container>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#808080",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
  colorWhite: {
    color: "#fff"
  },
});

export default HomeTab;
