import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  Alert,

} from "react-native";
import Moment from 'moment';
import Constants from 'expo-constants'
import { Button, Content } from "native-base";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class CurrentPackage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CustomerID:null,
      package_data:[],
      NoOfPieces:null,
      lat:null,
      long:null,
    };
  }

  componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          CustomerID: val.CustomerID ,
          lat: val.Lat ,
          long: val.Long ,
         
        });
        fetch(URL + "CustomerPackage/GetByCustomerID", {
          method: "GET",
          headers: {
           AuthToken:TOKEN,
           CustomerID:this.state.CustomerID,
          }
        })
          .then(res => res.json())
          .then(async response => {
            if (response.ResponseCode == 200) {
              this.setState({ isLoading: false, 
                package_data : response.list });
    
                console.log(this.state.package_data)
            } 
             else {
              this.setState({ isLoading: false });
            }
      
          })
          .catch(error => alert("Please Check Your Internet Connection"));
      }
    
    });
  
  }
  // postOrder = () => {
  //      console.log(this.state.package_data)
  //   fetch(URL + "Order/AddOrder", {
  //         method: "POST",
  //         headers: {
  //          AuthToken:TOKEN,
  //          CustomerID:this.state.CustomerID,
  //          IsPackage:true,
  //          IsPromotion:false,
  //          IsNormalDelivery:false,
  //          Lat:this.state.lat,
  //          Long:this.state.long,
  //          Note:"",
  //          PromotionID:null,
  //          ItemID:"",
  //          ItemQuantity:"",
  //          NoOfPieces:this.state.NoOfPieces,
  //          PackageID: this.state.package_data.PackageID
  //         }
  //       })
  //         .then(res => res.json())
  //         .then(async response => {
  //           console.log(response);
  //           if (response.ResponseCode == 200) {
  //             this.setState({ isLoading: false });
  //             alert("Your order placed successfully. Rider will be at your place soon !")
  //             this.props.navigation.push("ActionPage")
  //           } 
  //            else {
  //             Alert.alert(
  //               'Sorry',
  //               response.MsgToShow,
  //               [
  //                 {text: 'OK'},
  //               ],
  //               {cancelable: true},
  //             );
  //             this.setState({ isLoading: false });
  //           }
      
      
  //         })
  //         .catch(error => alert("Please Check Your Internet Connection"));
  //     }
  render() {
  
    const { navigation } = this.props.navigation;
    const { email, password } = this.state;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />
      <Content>
      {this.state.package_data.length < 1 ? (
        <View
            style={{
              alignItems: "center",
              height:deviceHeight,
              width:deviceWidth,
              alignContent:"center",
              paddingHorizontal:30,
              justifyContent:"center",
            }}
          >
        <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                 You are currently not subscribed to any package
                </Text>
        

            {/* ............Button................. */}
          </View>
      ):(
        this.state.package_data.map((item,index)=>{
         
       return(
        <View
        key={index}
            style={{
              alignItems: "center",
              paddingTop: 50,
            }}
          >
          <View style={{width:deviceWidth, alignItems:'center', alignContent:"center", paddingHorizontal:30}}>
             <Text style={{ fontSize: 40, color: "#000", fontFamily:"neuron_regular" }}>
           {item.PackageName}
              </Text>

             </View>
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center", paddingHorizontal:30}}>
             <Text style={{ fontSize: 32, color: "#000", fontFamily:"neuron_regular" }}>
             {item.Service ? item.Service : null} 
              </Text>

             </View>
            <View style={{width:deviceWidth, borderBottomColor:"#808080", borderBottomWidth:0.5, paddingBottom:50}}>
            
              <View style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  Remaining Items = {item.RemainingPieces}
                 
                </Text>
              </View>
              
              </View>
              <View style={{flexDirection:'row',marginTop:'5%',}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  {Moment(item.StartDate).format('MMM DD')}
                </Text>
              </View>
              </View>
              <View style={{flexDirection:'row',marginTop:'5%',}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  End Date = {Moment(item.EndDate).format('MMM DD')}
                </Text>
              </View>
              </View>
              <View style={{width:deviceWidth, marginTop:'5%', alignItems:"center", alignContent:"center"}}>
              <View style={{width:'60%'}}>
              <Button
                onPress={() => this.props.navigation.navigate("OrderFromPackage",{
                  PackageID: item.PackageID,
                  PackageName: item.PackageName
                })}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                   Order
                  </Text>
                </Button>
              </View>
              </View>
              
              </View>
            
     
          </View>
       
       )
      })
      )}
    
         
         {/* ............Button................. */}


  </Content>


       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default CurrentPackage;
