//import React from 'react';

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image, Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Content } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class SubcriptionOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delivery_type:"normal",
      CustomerID: null,
      email: "",
      address:"",
      isNormal:true,
      lat:null,
      lng:null,
      note:"",
    };
    this.data = this.props.navigation.state.params;
    console.log(this.data.item_ids + " ---" + this.data.item_quantities)
  }
  onSelect(index, value){
    alert(value)
    this.setState({
      delivery_type: value,
      isNormal: !this.state.isNormal

    })
  }
  postOrder = () => {
    if(!this.data.total_price || this.data.total_price == 0){
      Alert.alert(
        'Sorry',
        "Your cart is empty !",
        [
          {text: 'OK'},
        ],
        {cancelable: true},
      );
      }
      else{
        fetch(URL + "CustomerPackage/AddCustomerPackage", {
          method: "POST",
          headers: {
           AuthToken:TOKEN,
           CustomerID:this.state.CustomerID,
           PackageID:this.data.item_ids,
           NoOfPerson:this.data.item_quantities
          }
        })
          .then(res => res.json())
          .then(async response => {
            console.log(response);
            if (response.ResponseCode == 200) {
              this.setState({ isLoading: false });
              Alert.alert(
                'Congratulations !',
                "Package has been Subscribed",
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.props.navigation.push("ActionPage")
            } 
             else {
              Alert.alert(
                'Sorry',
                response.MsgToShow,
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.setState({ isLoading: false });
            }
      
      
          })
          .catch(error => alert("Please Check Your Internet Connection"));
      }

  }
  componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          CustomerID: val.CustomerID ,
          email: val.Email ,
          address:val.Address,
          lat:val.Lat,
          lng:val.Long,
        });
      }
    });
  }
  render() {
    const { navigation } = this.props.navigation;
    
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />
       <Content>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}>
               Order Now
              </Text>

             </View>
            <View style={{width:deviceWidth}}>
              <View style={{flexDirection:'row',marginTop:10,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080'}}>
                  Contact Info
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080'}}>
                  {this.state.email}
                </Text>
              </View>
              </View>
             
              { 
                    this.data.order_data.map((item, index)=>{
                      if(item.quantity) 
                        return (
                          <View key={index} style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                 {item.quantity} {item.Name}
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                 Rs. {item.Price}
                </Text>
              </View>
              </View>
                        )
                        else
                        return;
                    })
              }
              
              
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                  Total Items = {this.data.total_items}
                </Text>
              </View>
              </View>
            
             
              <View style={{width:'100%',marginTop:15}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                  Total Amount = RS. {this.data.total_price}
                </Text>
              </View>
              

             </View>
             <View style={{ width: deviceWidth,paddingRight:60,marginTop:30,paddingLeft:30 }}>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
                    All packages will be paid in advance </Text>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
Laundry collection and delivery will be done twice a week
 </Text>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
Collection and delivery timing will be between 9 am till 6 pm

 </Text>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >

Excluded items:

 </Text>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >


Men: jackets & Suits 


 </Text>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >


Women: lahanga, sari, fancy suits & jackets


 </Text>
               </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100
              }}
            >
              
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                onPress={this.postOrder}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                   Subscribe
                  </Text>
                </Button>
              )}
              </View>
              
             
              </View>

            {/* ............Button................. */}
          </View>
       

      
          </Content>
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default SubcriptionOrder;
