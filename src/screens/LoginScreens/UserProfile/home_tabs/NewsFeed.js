//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  Image,
  Alert,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import Constants from 'expo-constants'
import { Button, Content, Container } from "native-base";
import AccountHeader from "../../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";
import { URL } from "../../../../components/Api";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class NewsFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      access_token: "",

      current_pass: "",
      new_pass: "",
      confirm_pass: "",
      current_pass_err: "",
      new_pass_err: "",
      confirm_pass_err: "",

      isLoading: false
    };
    this.navigate = this.props.navigation.navigate;
  }
  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      this.setState({
        user_id: val.data.id,
        access_token: val.data.access_token
      });
    });
  }
  submit = () => {
    const {
      current_pass,
      new_pass,
      confirm_pass,
      current_pass_err,
      new_pass_err,
      confirm_pass_err
    } = this.state;

    if (current_pass == "" && new_pass == "" && confirm_pass == "") {
      this.setState({
        current_pass_err: "Required",
        new_pass_err: "Required",
        confirm_pass_err: "Required"
      });
    }
    if (current_pass != "") {
      this.setState({
        current_pass_err: ""
      });
    } else {
      this.setState({
        current_pass_err: "Required"
      });
    }
    if (new_pass != "") {
      this.setState({
        new_pass_err: ""
      });
    } else {
      this.setState({
        new_pass_err: "Required"
      });
    }
    if (confirm_pass != "") {
      this.setState({
        confirm_pass_err: ""
      });
    } else {
      this.setState({
        confirm_pass_err: "Required"
      });
    }
    if (new_pass != current_pass) {
      this.setState({
        confirm_pass_err: ""
      });
    } else {
      this.setState({
        confirm_pass_err: "Password does not match"
      });
    }
    if (current_pass != "" && new_pass != "" && confirm_pass != "") {
      this.setState({isLoading:true})
      fetch(URL + "change-password", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "access-token": this.state.access_token
        },
        body: JSON.stringify({
          "user_id" : this.state.user_id,
          "current" : current_pass,
          "new" : new_pass,
          "confirm" : confirm_pass
        })
      })
        .then(res => res.json())
        .then(async response => {
          if (response.response == "success") {
            this.setState({ isLoading: false });
            Alert.alert(
              'Success',
              "Password Changed Successfully !",
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
           this.props.navigation.navigate('Settings')
          } 
          else if(response.response == "error"){
            this.setState({ isLoading: false });
            Alert.alert(
              'Sorry',
              response.message,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
          }
           else {
            this.setState({ isLoading: false });
            Alert.alert(
              'Expired',
              "Session Expired",
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
          }
        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }
  };
  

  render() {
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
       
        <ImageBackground
          source={require("../../../../../assets/bg.png")}
          style={{
            height: deviceHeight,
            width: deviceWidth,
            resizeMode: "contain",
          }}
        >
          <AccountHeader
            navigation={this.props.navigation}
         />
          <View
            style={{
              alignItems: "center",
              paddingTop: "10%"
            }}
          >
           
           <Text>Home</Text>
            {/* ............Button................. */}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default NewsFeed;
