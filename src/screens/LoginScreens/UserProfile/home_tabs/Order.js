//import React from 'react';

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
   Alert,
  ActivityIndicator,
  TouchableOpacity,
  TouchableHighlight,
  Modal
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Content, Icon, Form, Textarea } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delivery_type:"normal",
      CustomerID: null,
      email: "",
      phone: "",
      address:"",
      isNormal:true,
      lat:null,
      lng:null,
      note:"",
      urgent_disabled:false,
      normal_disabled: true,
      order_data:[],
      item_ids:[],
      item_quantities:[],
      total_price:null,
      discount_price:null,
      total_items:null,
      modalVisible: false,


     
    };
    this.data = this.props.navigation.state.params;
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  onSelect(index, value){
    if(value == 'urgent'){
      alert("50% Rates will be increased on next day delivery")
     
      this.setState({total_price: this.state.total_price + this.state.discount_price, urgent_disabled : true, normal_disabled:false})
    }
    else{
      this.setState({total_price: this.state.total_price - this.state.discount_price, normal_disabled : true, urgent_disabled:false})
    }
    
    this.setState({
      delivery_type: value,
      isNormal: !this.state.isNormal

    })
  }
  postOrder = () => {
    if(this.state.total_price < 500){
      Alert.alert(
        'Sorry',
        "Your minimum order should be more than Rs. 500",
        [
          {text: 'OK'},
        ],
        {cancelable: true},
      );
      }
      else if(!this.state.total_price || this.state.total_price == 0){
        Alert.alert(
          'Success',
          "Your Cart is Empty",
          [
            {text: 'OK'},
          ],
          {cancelable: true},
        );
      }
      else{
        fetch(URL + "Order/AddOrder", {
          method: "POST",
          headers: {
           AuthToken:TOKEN,
           CustomerID:this.state.CustomerID,
           IsPackage:false,
           IsPromotion:false,
           IsNormalDelivery:this.state.isNormal,
           Lat:this.state.lat,
           Long:this.state.lng,
           Note:this.state.note,
           PromotionID:null,
           ItemID:this.state.item_ids,
           ItemQuantity:this.state.item_quantities,
           NoOfPieces:null,
          }
        })
          .then(res => res.json())
          .then(async response => {
            console.log(response);
            if (response.ResponseCode == 200) {
              this.setState({ isLoading: false });
              AsyncStorage.removeItem('total_price');
              AsyncStorage.removeItem('total_items');
              AsyncStorage.removeItem('selected_objects');
              Alert.alert(
                'Success',
                "Your order placed successfully. Rider will be at your place soon !",
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.props.navigation.push("ActionPage")
            } 
             else {
              Alert.alert(
                'Sorry',
                response.MsgToShow,
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.setState({ isLoading: false });
            }
      
      
          })
          .catch(error => alert("Please Check Your Internet Connection"));
      }

  }
  componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          CustomerID: val.CustomerID ,
          email: val.Email ,
          address:val.Address,
          lat:val.Lat,
          lng:val.Long,
          phone:val.Phone,
        });
      }
     
    });
    AsyncStorage.getItem("total_price").then(price_data => {
      const price_total = JSON.parse(price_data);
     
      if(price_total){
        this.setState({total_price : price_total, discount_price: price_total / 2 })
      }
    });
    AsyncStorage.getItem("total_items").then(total_items_data => {
      const total_items_count = JSON.parse(total_items_data);
     
      if(total_items_count){
        this.setState({total_items : total_items_count})
      }
    });
   
    AsyncStorage.getItem("selected_objects").then(order_data => {
      const val = JSON.parse(order_data);
     
      if(val){
        var itemIds_arr = [];
        var itemQuantity_arr = [];
        val.forEach(item => {
          if(item.quantity){
            itemIds_arr.push(item.ItemID);
            itemQuantity_arr.push(item.quantity);
          }
})


this.setState({order_data : val, item_ids: itemIds_arr, item_quantities: itemQuantity_arr })
      }
    });
  }
  render() {
    const { navigation } = this.props.navigation;
    console.log(this.state.order_data)
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />
      <Modal
            animationType={"fade"}
            transparent={true}
            onRequestClose={() => this.setModalVisible(false)}
            visible={this.state.modalVisible}
          >
            <View style={styles.popupOverlay}>
              <View style={styles.popup}>
                <View style={styles.popupContent}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setState({ modalVisible: false });
                    }}
                  >
                    <View
                      style={{
                        marginLeft: "auto",
                        alignContent: "center",
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                        borderRadius: 10,
                        width: 30,
                        height: 30,
                        backgroundColor: "#000"
                      }}
                    >
                      <Icon style={{ color: "#fff" }} name="ios-close" />
                    </View>
                  </TouchableHighlight>

                  <View>
                       <View
                        style={{
                          width: "90%",
                          alignContent: "center",
                          marginLeft: 10
                        }}
                      >
                        <Form>
                          
                            <Textarea
                              style={{
                                borderColor: "#fff",
                                borderWidth: 1,
                                paddingTop: 5
                              }}
                              onChangeText={note =>
                                this.setState({ note: note })
                              }
                              value={this.state.note}
                              rowSpan={5}
                              bordered
                              placeholder="Add Note"
                            />
                         

                        </Form>
                        
                      </View>

                    
                      <View
                        style={{
                          flexDirection: "row",
                          width: "100%",
                          paddingBottom: 40
                        }}
                      >
                          <View
                          style={{
                            paddingVertical: 20,
                            width: 100,
                            marginLeft: "auto",
                            marginRight: 10
                          }}
                        >
                      
                          <Button
                             onPress={() => {
                  this.setModalVisible(false);
                }}
                            bordered
                            dark
                            style={{
                              width: "100%",
                              height: 30,
                              backgroundColor: "#45c8d0",
                              borderRadius: 18,
                              borderColor:"#45c8d0"
                            }}
                          >
                            <View
                                style={{
                                  width: "100%",
                                  alignItems: "center",
                                  alignContent: "center"
                                }}
                              >
                              <Text style={{ textAlign: "center", fontFamily:"neuron_regular", fontSize:12 }}>
                                  ADD NOTE
                                </Text>
                                
                              </View>
                          </Button>
                         
                        </View>
                      </View>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
<Content>

       <View
            style={{
              alignItems: "center",
              paddingTop: "15%",
              paddingBottom:90
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}>
               Order Now
              </Text>

             </View>
            <View style={{width:deviceWidth}}>
              <View style={{flexDirection:'row',marginTop:10,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  Contact Info
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  {this.state.email}
                </Text>
              </View>
                
               
              </View>
              <View style={{flexDirection:'row',marginTop:10,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  Phone
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  {this.state.phone}
                </Text>
              </View>
                
               
              </View>
              <View style={{flexDirection:'row',marginTop:5,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontFamily:'neuron_regular',color:'#808080'}}>
                  Delivery Address
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080'}}>
                  {this.state.address}
                </Text>
              </View>
              </View>
              { 
                    this.state.order_data.map((item, index)=>{
                      if(item.quantity)
                        return (
                          <View key={index} style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                 {item.quantity} {item.Name}
                </Text>
              <Text style={{textAlign:'center',fontSize:14,fontFamily:'neuron_regular',color:"#808080"}}>
                ( {item.service} )
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                 Rs. {item.quantity * item.UnitPrice}
                </Text>
              </View>
              </View>
                        )
                        else
                        return;
                    })
              }
              
              
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                  Total Items = {this.state.total_items}
                </Text>
              </View>
              </View>
            
             
              <View style={{width:'100%',marginTop:15}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                  Total Amount = RS. {this.state.total_price}
                </Text>
              </View>
              <View style={{width:'100%'}}>
              <RadioGroup style={{flexDirection:'row'}}
               selectedIndex={0}
        onSelect = {(index, value) => this.onSelect(index, value)}
        
      >
        <RadioButton disabled={this.state.normal_disabled} value={'normal'} style={{paddingLeft:20}}>
          <Text>Normal Delivery</Text>
        </RadioButton>
 
        <RadioButton disabled={this.state.urgent_disabled} value={'urgent'} style={{paddingLeft:20}}>
          <Text>Next day delivery</Text>
        </RadioButton>
      </RadioGroup>
              </View>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100
              }}
            >
              
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ): (
                <Button
                onPress={this.postOrder}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Order Now
                  </Text>
                </Button>
              )}
              </View>
              
              <View style={{ width: deviceWidth,paddingRight:60,marginTop:10 }}>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Do check your clothes and their pockets for any belongings like (cash/documents)
                    before handing over to the Washman Team, No claims will be acceptable for any belongings later on.
                  </Text>
               </View>
               <TouchableOpacity
           onPress={() => {
                  this.setModalVisible(true);
                }}
                >
          <View style={{marginTop:20}}>
<Text style={{textAlign:"center", color:"#0000FF", fontFamily:"neuron_regular", fontSize:18}}>Write a note for rider Add Note</Text>
               </View>
               </TouchableOpacity>
              </View>
              
            {/* ............Button................. */}
          </View>
          
          </Content>
          
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#eff0f1",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7,
    borderColor:"#808080",
    borderWidth:0.7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
});

export default Order;
