import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
} from "react-native";
import { Content } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
class TermsandConditions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user1_id: "",
      company_name: "",
      person_name: "",
      access_token: "",
      isLoading: false
    };
    this.data = this.props.navigation.state.params;
  }

  render() {
    return (
      <View style={styles.container}>
        <BaseHeader
          navigation={this.props.navigation}
          back={true}
        />

        <View style={{ paddingTop: "5%", height: (deviceHeight * 2) / 2 }}>
          <Content
            ref={scroll => {
              this.scroll_view = scroll;
            }}
          >
            <View
              style={{ paddingLeft: "5%", paddingRight: 10, paddingBottom: 100 }}
            >
              <Text
                style={{ fontSize: 22, fontWeight: "400", color: "#000000" }}
              >
                Terms and conditions/ Terms of Use
              </Text>
              <Text
                style={styles.para}
              >
                These terms apply to all visitors, users and others who access or use the services. If you disagree with any part of the terms then you may not access the services.
              </Text>
              <Text
                style={styles.para}
              ></Text>
              <Text
                style={styles.para}
              >
                Some parts of the services are built on a subscription(s) basis.
              </Text>
              <Text
                style={styles.para}
              ></Text>
              <Text
               style={styles.para}
              >
                Always share the accurate details for the subscriber like name, email address, contact number, pick up and delivery address, so the subscriber can utilize the services accordingly.
              </Text>
              <Text
                style={styles.para}
              ></Text>
              <Text
             style={styles.para}
              >
               Validity for available packages is Thirty (30) days only, any leftover items/ units after the expiry date will not carry forward or transferable to the next month or with the renewal of next package.
              </Text>
              <Text
                style={styles.para}
              >
              </Text>

              <Text
                style={styles.para}
              >
                Washman reserves all the rights to change or modify or replace or cancel or edit any subscription or package or any terms at anytime.
              </Text>
              <Text
                style={styles.para}
              ></Text>
              <Text
                style={styles.para}
              >
If a laundry service customer misses their weekly drop off of dirty laundry, the customer will forfeit the unused laundry service for that week and will not be reimbursed.

              </Text>
              <Text
                 style={styles.para}
              >
              </Text>
              <Text
               style={styles.para}
              >
                The customer must pay the agreed-upon charges prior to service.

              </Text>
              <Text
                style={styles.para}
              >
              </Text>
              <Text
                style={styles.para}
              >
                Non package Users should pay the amount of the order at the time of delivery.
              </Text>
              <Text
                 style={styles.para}
              >
              </Text>
              <Text
               style={styles.para}
              >
                The customer hereby grants Washman the right to withhold the customer’s clothing until payment is made for the excess laundry or pickup.
              </Text>
              <Text
                style={styles.para}
              >
              </Text>

              <Text
                style={styles.para}
              >
                Timing for the Pickup and delivery services will be between 9am till 6pm, 7 days a week.
              </Text>

              <Text
                 style={styles.para}
              >
              </Text>
              <Text
                 style={styles.para}
              >
                Normal delivery time would be three (3) to four (4) days.
              </Text>
              <Text
                 style={styles.para}
              >
              </Text>
              <Text
              style={styles.para}
              >
                Due to unavoidable circumstances or the natural disasters or any unseen or severe weather conditions, the services might get delayed.
              </Text>
              <Text
              style={styles.para}
              >
              </Text>
              <Text
              style={styles.para}
              >
               Incase of lost of the clothes/ item, Washman will pay five times of the  price of the item’s bill.
              </Text>
              <Text
              style={styles.para}
              >
              </Text>
              <Text
              style={styles.para}
              >
               During the processing, incase if there is any damage to the clothes or its color, Washman will not be responsible for any loss.
              </Text>
              
              {/* .............. */}
            
              </View>
          </Content>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#EBEBEB"
  },
  formContent: {
    flexDirection: "row",
    marginTop: 30
  },
  inputContainer: {
    borderBottomColor: "#F5FCFF",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    margin: 10
  },
  icon: {
    width: 30,
    height: 30
  },
  iconBtnSearch: {
    alignSelf: "center"
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: "#FFFFFF",
    flex: 1
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: "center"
  },
  notificationList: {
    marginTop: 20,
    padding: 10
  },
  notificationBox: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 5,
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    borderRadius: 10
  },
  image: {
    width: 45,
    height: 45,
    borderRadius: 20,
    marginLeft: 20
  },
  name: {
    fontSize: 16,
    color: "#808080",
    marginLeft: 10,
    alignSelf: "center"
  },
  main_heading:{
    fontSize: 18,
    fontWeight: "300",
    color: "#000",
    marginTop: 5
  },
  heading:{
    fontSize: 16,
    fontWeight: "100",
    color: "#000",
    marginTop: 5
  },
  para:{
    fontSize: 12,
    fontWeight: "100",
    color: "#808080",
    marginTop: 5,
    lineHeight: 25
  },
  para_imp:{
    fontSize: 14,
    fontWeight: "100",
    color: "red",
    marginTop: 5,
    lineHeight: 25
  }
});

export default TermsandConditions;
