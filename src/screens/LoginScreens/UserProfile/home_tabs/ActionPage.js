//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  Modal,
} from "react-native";
import Constants from "expo-constants";
import {
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Textarea,
  Form,
  Footer, FooterTab,
  Container
} from "native-base";

import AccountHeader from "../../../../components/AccountHeader";
import buttonImg from "../../../../../assets/button.png";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class ActionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      image: null,
      errorMessage:"",
      location:null
    };
    this.navigate = this.props.navigation.navigate;
  }
 
  render() {
    let { image } = this.state;
    return (
        <Container>
       <AccountHeader navigation={this.props.navigation} />
       <View
            style={{
              width: deviceWidth,
              paddingTop: 20,
              paddingBottom: 20,
              alignItems: "center",
              alignContent: "center"
            }}
          >
         
           <Image style={{height:200, width:deviceWidth, resizeMode:"cover", paddingHorizontal:50}} source={require("../../../../../assets/header_white.png")}/>

          </View>
          <Content>
          <View style={{width:deviceWidth, alignContent:"center", alignItems:"center"}}>
          <View style={{width:"90%"}}>
          <TouchableOpacity
          onPress={() => this.props.navigation.push("AvailablePromotions")}>
          <ImageBackground style={{width:"100%", height:60, alignItems: "flex-start",justifyContent: "center",paddingLeft:20}} source={buttonImg}>
          <Text style={{ fontSize: 26, color: "#fff", fontFamily:"neuron_bold" }}>
               TODAY'S PROMOTION
              </Text>
              
          </ImageBackground>
          </TouchableOpacity>
         
          </View>
          </View>
          <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", marginTop:25}}>
          
          <View style={{width:"90%"}}>
          <TouchableOpacity  onPress={() => this.props.navigation.push("HomeTab")}>
          <ImageBackground style={{width:"100%", height:60, alignItems: "flex-start",justifyContent: "center",paddingLeft:20}} source={buttonImg}>
          <Text style={{ fontSize: 26, color: "#fff", fontFamily:"neuron_bold" }}>
               PLACE A NEW ORDER
              </Text>
          </ImageBackground>
          </TouchableOpacity>
          </View>
         
          </View>
          <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", marginTop:25}}>
          <View style={{width:"90%"}}>
          <TouchableOpacity  onPress={() => this.props.navigation.push("PackageSubscription")}>
          <ImageBackground style={{width:"100%", height:60, alignItems: "flex-start",justifyContent: "center",paddingLeft:20}} source={buttonImg}>
          <Text style={{ fontSize: 26, color: "#fff", fontFamily:"neuron_bold" }}>
               MONTHLY PACKAGES
              </Text>
              
          </ImageBackground>
          </TouchableOpacity>
          </View>
          </View>
          
          {/* ............. */}
    
          </Content>
         

          <Footer>
          <FooterTab style={{backgroundColor:"#1c2631"}}>
            <Button vertical>
              <Icon style={styles.colorWhite} name="home" />
            </Button>
            <Button vertical 
          onPress={() => {
                this.props.navigation.push("CustomerSupport");
              }}
              >
              <Icon style={styles.colorWhite} name="mail" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("OrderHistory");
              }}
              >
              <Icon style={styles.colorWhite} active name="list-box" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("CurrentPackage");
              }}
              >
              <Icon style={styles.colorWhite} active name="cube" />
            </Button>
          
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("Settings");
              }}
              >
              <Icon style={styles.colorWhite} active name="settings" />
            </Button>
            
          </FooterTab>
        </Footer>

      </Container>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#808080",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
  colorWhite: {
    color: "#fff"
  },
});

export default ActionPage;
