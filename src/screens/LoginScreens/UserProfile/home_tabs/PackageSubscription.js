//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import Constants from "expo-constants";
import {
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Container
} from "native-base";

import AccountHeader from "../../../../components/AccountHeader";
import buttonImg from "../../../../../assets/button.png";
import { URL, TOKEN } from "../../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class PackageSubscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: "",
      packages_data: [],
      items: [],
      items_quantity: [],
      order: [],
      total_price: 0,
      total_items: 0
    };
    this.navigate = this.props.navigation.navigate;
  }
  componentWillMount() {
    fetch(URL + "Package/Get", {
      method: "GET",
      headers: {
        AuthToken: TOKEN
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.ResponseCode == 200) {
          this.setState({ isLoading: false, packages_data: response.list });
        } else {
          Alert.alert("Sorry", response.MsgToShow, [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  }
  removeItem = item_id => {
    const result = this.state.items.findIndex(task => task === item_id);
    var indexOfItem = this.state.packages_data.find(
      task => task.PackageID === item_id
    );

    var ItemPrice = parseInt(indexOfItem.Price);
    this.setState({
      total_price: this.state.total_price - ItemPrice / indexOfItem.quantity,
      total_items: this.state.total_items - 1
    });
    this.state.items_quantity[result]--;

    if (indexOfItem.quantity <= 1) {
      indexOfItem.quantity--;
      this.state.items.pop(result);
      this.state.items_quantity.pop(result);
    } else {
      indexOfItem.Price =
        indexOfItem.Price - indexOfItem.Price / indexOfItem.quantity;
      indexOfItem.NoOfPieces = indexOfItem.NoOfPieces - indexOfItem.one_piece;
      indexOfItem.quantity--;
    }

    this.setState({
      packages_data: this.state.packages_data
    });
  };
  addItem = item_id => {
    const result = this.state.items.findIndex(task => task === item_id);
    var indexOfItem = this.state.packages_data.find(
      task => task.PackageID === item_id
    );
    this.state.order.push(indexOfItem);

    var ItemPrice = parseInt(indexOfItem.Price);

    if (result < 0) {
      this.state.items.push(item_id);
      this.state.items_quantity.push(1);
      indexOfItem["quantity"] = 1;
      indexOfItem["one_piece"] = indexOfItem.NoOfPieces;
      indexOfItem["one_price"] = indexOfItem.Price;
      this.setState({
        packages_data: this.state.packages_data,
        total_price: parseInt(this.state.total_price) + ItemPrice,
        total_items: this.state.total_items + 1
      });
    } else {
      this.state.items_quantity[result]++;
      indexOfItem.quantity++;
      indexOfItem.Price =
        parseInt(indexOfItem.Price) + parseInt(indexOfItem.one_price);
      indexOfItem.NoOfPieces =
        parseInt(indexOfItem.NoOfPieces) + parseInt(indexOfItem.one_piece);
      this.setState({
        packages_data: this.state.packages_data,
        total_price:
          parseInt(this.state.total_price) + parseInt(indexOfItem.one_price),
        total_items: this.state.total_items + 1
      });
    }
  };
  render() {
    let { image } = this.state;
    return (
      <Container>
        <AccountHeader navigation={this.props.navigation} back={true} />
        <View
          style={{
            width: deviceWidth,
            paddingTop: 20,
            paddingBottom: 20,
            alignItems: "center",
            alignContent: "center"
          }}
        >
          <Image
            style={{
              height: 120,
              width: "90%",
              resizeMode: "cover",
              paddingHorizontal: 50
            }}
            source={require("../../../../../assets/header_white.png")}
          />
        </View>
        <Content>
          {this.state.packages_data.map((item, index) => {
            return (
              <View
                key={index}
                style={{
                  width: deviceWidth,
                  alignContent: "center",
                  alignItems: "center",
                  marginVertical: 10
                }}
              >
                <View style={{ width: "90%", backgroundColor: "#1c2631" }}>
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 30,
                      fontFamily: "neuron_regular"
                    }}
                  >
                    {" "}
                    {item.Name}
                  </Text>
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 24,
                      fontFamily: "neuron_regular"
                    }}
                  >
                    {" "}
                    {item.Service}
                  </Text>
                  <View style={{ paddingVertical: 5 }}>
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 14,
                        fontFamily: "neuron_regular"
                      }}
                    >
                      {" "}
                      {item.Description}
                    </Text>
                  </View>
                  <View style={{ width: "100%", backgroundColor: "##5D6D7E" }}>
                    <View style={{ flexDirection: "row", color: "#fff" }}>
                      <View style={{ flexDirection: "row" }}>
                        {item.quantity ? (
                          <TouchableOpacity
                            onPress={() => this.removeItem(item.PackageID)}
                          >
                            <View
                              style={{
                                height: 30,
                                width: 30,
                                alignItems: "center"
                              }}
                            >
                              <Text
                                style={{
                                  color: "#fff",
                                  fontSize: 24,
                                  textAlign: "center"
                                }}
                              >
                                -
                              </Text>
                            </View>
                          </TouchableOpacity>
                        ) : null}
                        <View
                          style={{
                            height: 30,
                            alignItems: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: "#fff",
                              paddingLeft: 5,
                              fontSize: 24,
                              textAlign: "center"
                            }}
                          >
                            {item.quantity ? item.quantity : 0}
                          </Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => this.addItem(item.PackageID)}
                        >
                          <View
                            style={{
                              height: 30,
                              width: 30,
                              alignItems: "center"
                            }}
                          >
                            <Text
                              style={{
                                color: "#fff",
                                fontSize: 24,
                                textAlign: "center"
                              }}
                            >
                              +
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={{ marginLeft: "auto", paddingRight: 5 }}>
                        <Text style={{ color: "#fff" }}>Rs.{item.Price}</Text>
                      </View>
                    </View>
                    <View style={{flexDirection:"row"}}>
                    <View>
                      <Text style={{ color: "#fff", paddingLeft: 5 }}>
                        {parseInt(item.NoOfPieces)} Pcs
                      </Text>
                    </View>
                    <View>
                      <Text style={{ color: "#fff", paddingLeft: 5 }}>
                       - {parseInt(item.NoOfDays)} Days
                      </Text>
                    </View>
                    </View>
                    
                  </View>
                </View>
              </View>
            );
          })}
        </Content>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("SubcriptionOrder", {
              total_items: this.state.total_items,
              total_price: this.state.total_price,
              order_data: this.state.packages_data,
              item_ids: this.state.items,
              item_quantities: this.state.items_quantity
            })
          }
        >
          <View
            style={{
              width: deviceWidth,
              alignContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width: "90%",
                backgroundColor: "#45c8d0",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "stretch",
                marginBottom: 10,
                paddingVertical: 10,
                paddingHorizontal: 5
              }}
            >
              <View>
                <Text style={{ color: "#fff", marginLeft: 5 }}>
                  {this.state.total_items}
                </Text>
              </View>
              <View>
                <Text
                  style={{
                    color: "#fff",
                    fontSize: 20,
                    fontFamily: "neuron_regular"
                  }}
                >
                  View Your Cart
                </Text>
              </View>
              <View>
                <Text style={{ color: "#fff", marginRight: 5 }}>
                  Rs.{this.state.total_price}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <Footer>
          <FooterTab style={{backgroundColor:"#1c2631"}}>
            <Button vertical
            onPress={() => {this.props.navigation.push("ActionPage")}}>
              <Icon style={styles.colorWhite} name="home"/>
            </Button>
            <Button vertical 
          onPress={() => {
                this.props.navigation.push("CustomerSupport");
              }}
              >
              <Icon style={styles.colorWhite} name="mail" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("OrderHistory");
              }}
              >
              <Icon style={styles.colorWhite} active name="list-box" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("CurrentPackage");
              }}
              >
              <Icon style={styles.colorWhite} active name="cube" />
            </Button>
            <Button vertical 
            onPress={() => {
                this.props.navigation.push("Settings");
              }}
              >
              <Icon style={styles.colorWhite} active name="settings" />
            </Button>
            
          </FooterTab>
        </Footer>
  </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#808080",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
  colorWhite: {
    color: "#fff"
  }
});

export default PackageSubscription;
