//import React from 'react';

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Container } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class MonthlyPackage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delivery_type:"urgent",
    };
  }
  onSelect(index, value){
    alert(value)
    this.setState({
      delivery_type: value
    })
  }
 
  
  
  submit = () => {
    const { email, password } = this.state;
    let email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email == "" && password == "") {
      this.setState({
        email_err: "Email should not be empty",
        password_err: "Password should not be empty"
      });
    }
    if (email != "") {
      if (email_reg.test(email) === false) {
        this.setState({ email_err: "Invalid Email" });
      } else {
        this.setState({ email_err: "" });
      }
    }
    if (password == "") {
      this.setState({ password_err: "Required" });
    } else {
      this.setState({
        password_err: ""
      });

    }
    if(email != "" && password != ""){
      this.login_form(email,password);
    }
  };
  render() {
    const { navigation } = this.props.navigation;
    const { email, password } = this.state;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />

       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 40, color: "#000", fontFamily:"neuron_regular" }}>
             Monthly Package
              </Text>

             </View>
            <View style={{width:deviceWidth}}>
              <View style={{flexDirection:'row',marginTop:10,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  Contact Info
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080'}}>
                  abc@gmail.com
                </Text>
              </View>
                
               
              </View>
              <View style={{flexDirection:'row',marginTop:5,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontFamily:'neuron_regular',color:'#808080'}}>
                  Delivery Address
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080'}}>
                  house, sector14
                </Text>
              </View>
              </View>
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                 You Selected basic package
                </Text>
              </View>
              </View>
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  Total Persons = 4
                </Text>
              </View>
              </View>
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  Total Items = 4
                </Text>
              </View>
              </View>
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  Started Date = 01-01-2020
                </Text>
              </View>
              </View>
            
             
              <View style={{width:'100%',marginTop:15}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                  Total Amount = RS.5000
                </Text>
              </View>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100
              }}
            >
              
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                  // onPress={this.submit}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Order Now
                  </Text>
                </Button>
              )}
              </View>
              
              <View style={{ width: deviceWidth,paddingRight:60,marginTop:50 }}>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Do check your clothes and their pockets for any belongings like (cash/documents)
                    before handing over to the Washman Team, No claims will be acceptable for any belongings later on.
                  </Text>
               </View>
              </View>

            {/* ............Button................. */}
          </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default MonthlyPackage;
