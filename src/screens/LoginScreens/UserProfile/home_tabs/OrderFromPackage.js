import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  Alert,

} from "react-native";
import Moment from 'moment';
import Constants from 'expo-constants'
import { Button, Content, Toast } from "native-base";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class OrderFromPackage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CustomerID:null,
      package_data:[],
      NoOfPieces:"",
      lat:null,
      long:null,
    };
    this.data = this.props.navigation.state.params;
    console.log(this.data)
  }
 
  componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          CustomerID: val.CustomerID ,
          lat: val.Lat ,
          long: val.Long ,
         
        });
      }
    });
  
  }
  postOrder = () => {
    fetch(URL + "Order/AddOrder", {
          method: "POST",
          headers: {
           AuthToken:TOKEN,
           CustomerID:this.state.CustomerID,
           IsPackage:true,
           IsPromotion:false,
           IsNormalDelivery:false,
           Lat:this.state.lat,
           Long:this.state.long,
           Note:"",
           PromotionID:null,
           ItemID:"",
           ItemQuantity:"",
           NoOfPieces:this.state.NoOfPieces,
           PackageID: this.data.PackageID
          }
        })
          .then(res => res.json())
          .then(async response => {
            console.log(response);
            if (response.ResponseCode == 200) {
              this.setState({ isLoading: false, NoOfPieces:"" });
              Alert.alert(
                'Success',
                "Your order placed successfully. Rider will be at your place soon !",
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.props.navigation.push("ActionPage")
            } 
             else {
              Alert.alert(
                'Sorry',
                response.MsgToShow,
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.setState({ isLoading: false });
            }
      
      
          })
          .catch(error => alert("Please Check Your Internet Connection"));
      }
  render() {
    const { navigation } = this.props.navigation;
    const { email, password } = this.state;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />
      <Content>
  <View
            style={{
              alignItems: "center",
              paddingTop: "15%",
              paddingBottom:"15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center", paddingHorizontal:30}}>
             <Text style={{ fontSize: 40, color: "#000", fontFamily:"neuron_regular" }}>
             {this.data.PackageName} 
              </Text>

             </View>
           <View style={{width:deviceWidth}}>
            
              
              <View style={{marginTop:10, width:deviceWidth, alignContent:"center", alignItems:"center" }}>
              <View style={{width:"80%"}}>

              <TextField
                autoCapitalize="none"
                keyboardType={"numeric"}
                onChangeText={NoOfPieces => this.setState({ NoOfPieces })}
                value={this.state.NoOfPieces}
                autoCorrect={false}
                label="No of Pieces"
                maxLength={10}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
              </View>
              </View>
              <View style={{marginTop:10, width:deviceWidth, alignContent:"center", alignItems:"center" }}>
              <View style={{width:"60%"}}>
              <Button
                onPress={this.postOrder}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Place Order
                  </Text>
                </Button>
              </View>
              </View>
             </View>
            {/* ............Button................. */}
          </View>
</Content>


       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default OrderFromPackage;
