//import React from 'react';

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator,
  TouchableHighlight,
  Modal,
  
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Content, Toast, Icon, Form, Textarea } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class PromotionOrder extends Component {
  constructor(props) {
    super(props);
    this.data = this.props.navigation.state.params;
    this.state = {
      delivery_type:"normal",
      CustomerID: null,
      email: "",
      phone: "",
      address:"",
      isNormal:true,
      lat:null,
      lng:null,
      note:"",
      modalVisible:false,
      total_price:this.data.total_price,
      half_total : this.data.total_price / 2
    };
   
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  onSelect(index, value){
    if(value == "urgent"){
      Alert.alert(
        'Note',
        '50% will increase for next day delivery',
        [
          {text: 'OK'},
        ],
        {cancelable: false},
      );
      this.setState({
        total_price: this.state.total_price + this.state.half_total,
      })
    }
    else{
      this.setState({
        total_price : this.state.total_price - this.state.half_total
      })
    }
    this.setState({
      delivery_type: value,
      isNormal: !this.state.isNormal,
    })
  }
  postOrder = () => {
 if(!this.state.total_price || this.state.total_price == 0){
  Alert.alert(
    'Sorry',
    "Your cart is empty !",
    [
      {text: 'OK'},
    ],
    {cancelable: true},
  );
}
else{
  fetch(URL + "Order/AddOrder", {
    method: "POST",
    headers: {
     AuthToken:TOKEN,
     CustomerID:this.state.CustomerID,
     IsPackage:false,
     IsPromotion:true,
     IsNormalDelivery:true,
     Lat:this.state.lat,
     Long:this.state.lng,
     Note:this.state.note,
     PromotionID:this.data.item_ids,
     ItemID:null,
     ItemQuantity:this.data.item_quantities,
     NoOfPieces:null,
    }
  })
    .then(res => res.json())
    .then(async response => {
      console.log(response);
      if (response.ResponseCode == 200) {
        this.setState({ isLoading: false });
       
        alert("Your order placed successfully. Rider will be at your place soon !")
        this.props.navigation.push("ActionPage")
      } 
       else {
        Alert.alert(
          'Sorry',
          response.MsgToShow,
          [
            {text: 'OK'},
          ],
          {cancelable: true},
        );
        this.setState({ isLoading: false });
      }


    })
    .catch(error => alert("Please Check your Internet Connection"));
}
  }
  toast = () => {
    Toast.show({
      text: 'Your order placed successfully. Rider will be at your place soon !',
      duration: 3000
    });
  }
  componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          CustomerID: val.CustomerID ,
          email: val.Email ,
          address:val.Address,
          lat:val.Lat,
          lng:val.Long,
          phone:val.Phone,
        });
      }
     
    });
  
  }
  render() {
    const { navigation } = this.props.navigation;
    
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />
      <Modal
            animationType={"fade"}
            transparent={true}
            onRequestClose={() => this.setModalVisible(false)}
            visible={this.state.modalVisible}
          >
            <View style={styles.popupOverlay}>
              <View style={styles.popup}>
                <View style={styles.popupContent}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setState({ modalVisible: false });
                    }}
                  >
                    <View
                      style={{
                        marginLeft: "auto",
                        alignContent: "center",
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                        borderRadius: 10,
                        width: 30,
                        height: 30,
                        backgroundColor: "#000"
                      }}
                    >
                      <Icon style={{ color: "#fff" }} name="ios-close" />
                    </View>
                  </TouchableHighlight>

                  <View>
                       <View
                        style={{
                          width: "90%",
                          alignContent: "center",
                          marginLeft: 10
                        }}
                      >
                        <Form>
                          
                            <Textarea
                              style={{
                                borderColor: "#fff",
                                borderWidth: 1,
                                paddingTop: 5
                              }}
                              onChangeText={note =>
                                this.setState({ note: note })
                              }
                              value={this.state.note}
                              rowSpan={5}
                              bordered
                              placeholder="Add Note"
                            />
                         

                        </Form>
                        
                      </View>

                    
                      <View
                        style={{
                          flexDirection: "row",
                          width: "100%",
                          paddingBottom: 40
                        }}
                      >
                          <View
                          style={{
                            paddingVertical: 20,
                            width: 100,
                            marginLeft: "auto",
                            marginRight: 10
                          }}
                        >
                      
                          <Button
                             onPress={() => {
                  this.setModalVisible(false);
                }}
                            bordered
                            dark
                            style={{
                              width: "100%",
                              height: 30,
                              backgroundColor: "#45c8d0",
                              borderRadius: 18,
                              borderColor:"#45c8d0"
                            }}
                          >
                            <View
                                style={{
                                  width: "100%",
                                  alignItems: "center",
                                  alignContent: "center"
                                }}
                              >
                              <Text style={{ textAlign: "center", fontFamily:"neuron_regular", fontSize:12 }}>
                                  ADD NOTE
                                </Text>
                                
                              </View>
                          </Button>
                         
                        </View>
                      </View>
                  </View>
                </View>
              </View>
            </View>
          </Modal>

      <Content>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%",
              paddingBottom:100
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}>
               Order Now
              </Text>

             </View>
            <View style={{width:deviceWidth}}>
              <View style={{flexDirection:'row',marginTop:10,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  Contact Info
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  {this.state.email}
                </Text>
              </View>
                
               
              </View>
              <View style={{flexDirection:'row',marginTop:10,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  Phone
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  {this.state.phone}
                </Text>
              </View>
                
               
              </View>
              <View style={{flexDirection:'row',marginTop:5,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontFamily:'neuron_regular',color:'#808080'}}>
                  Delivery Address
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',color:'#808080',fontFamily:'neuron_regular'}}>
                  {this.state.address}
                </Text>
              </View>
              </View>
              { 
                    this.data.order_data.map((item, index)=>{
                      if(item.quantity)
                        return (
                          <View key={index} style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                 {item.quantity} {item.ItemName}
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                 Rs. {item.quantity * item.Price}
                </Text>
              </View>
              </View>
                        )
                        else
                        return;
                    })
              }
              
              
              <View style={{flexDirection:'row',marginTop:15,}}>
              <View style={{width:'100%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                  Total Items = {this.data.total_items}
                </Text>
              </View>
              </View>
            
             
              <View style={{width:'100%',marginTop:15}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_bold',}}>
                  Total Amount = RS. {this.state.total_price}
                </Text>
              </View>
              <View style={{width:'100%'}}>
              
              </View>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100
              }}
            >
              
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                onPress={this.postOrder}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Order Now
                  </Text>
                </Button>
              )}
              </View>
              
              <View style={{ width: deviceWidth,paddingRight:60,marginTop:10 }}>
      <Text
                    style={{
                      color: "#808080",
                      fontSize: 16,
                     
                      textAlign: "left",
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Do check your clothes and their pockets for any belongings like (cash/documents)
                    before handing over to the Washman Team, No claims will be acceptable for any belongings later on.
                  </Text>
               </View>
              </View>
              <TouchableOpacity
           onPress={() => {
                  this.setModalVisible(true);
                }}
                >
          <View style={{marginTop:20}}>
<Text style={{textAlign:"center", color:"#0000FF", fontFamily:"neuron_regular", fontSize:18}}>Write a note for rider Add Note</Text>
               </View>
               </TouchableOpacity>

            {/* ............Button................. */}
          </View>
       
          </Content>
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#eff0f1",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7,
    borderColor:"#808080",
    borderWidth:0.7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
});

export default PromotionOrder;
