//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  Image,
  Alert,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import Constants from 'expo-constants'
import { Button, Content, Container } from "native-base";
import BaseHeader from "../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL } from "../../../components/Api";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      access_token: "",

      current_pass: "",
      new_pass: "",
      confirm_pass: "",
      current_pass_err: "",
      new_pass_err: "",
      confirm_pass_err: "",

      isLoading: false
    };
    this.navigate = this.props.navigation.navigate;
  }
  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      this.setState({
        user_id: val.data.id,
        access_token: val.data.access_token
      });
    });
  }
  submit = () => {
    const {
      current_pass,
      new_pass,
      confirm_pass,
      current_pass_err,
      new_pass_err,
      confirm_pass_err
    } = this.state;

    if (current_pass == "" && new_pass == "" && confirm_pass == "") {
      this.setState({
        current_pass_err: "Required",
        new_pass_err: "Required",
        confirm_pass_err: "Required"
      });
    }
    if (current_pass != "") {
      this.setState({
        current_pass_err: ""
      });
    } else {
      this.setState({
        current_pass_err: "Required"
      });
    }
    if (new_pass != "") {
      this.setState({
        new_pass_err: ""
      });
    } else {
      this.setState({
        new_pass_err: "Required"
      });
    }
    if (confirm_pass != "") {
      this.setState({
        confirm_pass_err: ""
      });
    } else {
      this.setState({
        confirm_pass_err: "Required"
      });
    }
    if (new_pass != current_pass) {
      this.setState({
        confirm_pass_err: ""
      });
    } else {
      this.setState({
        confirm_pass_err: "Password does not match"
      });
    }
    if (current_pass != "" && new_pass != "" && confirm_pass != "") {
      this.setState({isLoading:true})
      fetch(URL + "change-password", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "access-token": this.state.access_token
        },
        body: JSON.stringify({
          "user_id" : this.state.user_id,
          "current" : current_pass,
          "new" : new_pass,
          "confirm" : confirm_pass
        })
      })
        .then(res => res.json())
        .then(async response => {
          if (response.response == "success") {
            this.setState({ isLoading: false });
            Alert.alert(
              'Success',
              "Password Changed Successfully !",
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
           this.props.navigation.navigate('Settings')
          } 
          else if(response.response == "error"){
            this.setState({ isLoading: false });
            Alert.alert(
              'Sorry',
              response.message,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
          }
           else {
            this.setState({ isLoading: false });
            Alert.alert(
              'Expired',
              "Session Expired",
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
          }
        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }
  };
  validation = () => {
    // const {contact_person, username, email,password, re_password} = this.state;
    // personName_regex = /^[a-zA-z]+$/
    // username_regex = /^[a-zA-Z0-9_]{1,15}$/
    // email_regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/
    // contact_person == '' ? alert('contact person is empty'):
    // (!personName_regex.test(contact_person)) ? alert('Please Enter correct Contact Person Name') :
    //  (username == '') ? alert('username is empty') :
    //  (!username_regex.test(username)) ? alert('Enter a Valid Username') :
    //   (email == '') ? alert('email is empty') :
    //   (!email_regex.test(email)) ? alert('email is invalid') :
    //   (password == '') ? alert('password is empty') :
    //   (password != re_password) ? alert('password not matched') :
    //   this.props.navigation.navigate("UserProfile_step2", {
    //     person_name: contact_person,
    //     username: username,
    //     email: email,
    //     password:password,
    //   })
  };

  render() {
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
       
        <ImageBackground
          source={require("../../../../assets/bg.png")}
          style={{
            height: deviceHeight,
            width: deviceWidth,
            resizeMode: "contain",
          }}
        >
         <BaseHeader
          PageTitle=""
          IconLeft="ios-arrow-back"
          drawerOpen="LoginHome"
          navigation={this.props.navigation}
        />
          <View
            style={{
              alignItems: "center",
              paddingTop: "10%"
            }}
          >
            <View
              style={{
                width: deviceWidth,
                alignItems: "center",
                alignContent: "center"
              }}
            >
              <Text style={{ fontSize: 26, fontWeight: "800", color: "#fff" }}>
                Change Password
              </Text>
            </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100,
                paddingTop: "5%"
              }}
            >
              <TextField
                autoCapitalize="none"
                onChangeText={current_pass => this.setState({ current_pass })}
                autoCorrect={false}
                label="Current Password"
                maxLength={40}
                tintColor={"#fff"}
                baseColor="#fff"
                textColor="#fff"
              />
              <TextField
                autoCapitalize="none"
                onChangeText={new_pass => this.setState({ new_pass })}
                autoCorrect={false}
                label="New Password"
                maxLength={40}
                tintColor={"#fff"}
                baseColor="#fff"
                textColor="#fff"
              />

              <TextField
                autoCapitalize="none"
                //labelHeight={heightPercentageToDP(1)}
                onChangeText={confirm_pass => this.setState({ confirm_pass })}
                autoCorrect={false}
                label="Confirm Password"
                maxLength={40}
                tintColor={"#fff"}
                baseColor="#fff"
                textColor="#fff"
              />

              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center"
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 4,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#DA2128",
                    backgroundColor: "#DA2128",
                    borderRadius: 25
                  }}
                >
                <ActivityIndicator size="small" />
                </Button>
              ) : (
                <Button
                  onPress={this.submit}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 4,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#DA2128",
                    backgroundColor: "#DA2128",
                    borderRadius: 25
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    Submit
                  </Text>
                </Button>
              )}
                
              </View>
              <View
                style={{
                  paddingVertical: 20,
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center"
                }}
              >
                <Image
                  source={require("../../../../assets/logofade.png")}
                  style={{ resizeMode: "contain", height: 90, width: 90 }}
                />
              </View>
            </View>

            {/* ............Button................. */}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default ChangePassword;
