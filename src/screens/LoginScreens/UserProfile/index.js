import React from 'react';
import { Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // 6.2.2
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import HomeTab from './home_tabs/ProfileHome';
import RecomendationTab from './home_tabs/Recomendations';
import NewsFeedTab from './home_tabs/NewsFeed';

class HomePage extends React.Component {
  
  render() {
    const navigation = this.props.navigation
    return (
      <HomeTab navigation = {navigation}/>
    );
  }
}
class NewsFeedPage extends React.Component {
  render() {
    const navigation = this.props.navigation
    return (
      <NewsFeedTab navigation = {navigation}/>
    );
  }
}
class RecomendationPage extends React.Component {
  render() {
    const navigation = this.props.navigation
    return (
      <RecomendationTab navigation = {navigation}/>
    );
  }
}




class IconWithBadge extends React.Component {
  render() {
    const { name, badgeCount, color, size } = this.props;
    return (
      <View style={{ width: 24, height: 24, margin: 5 }}>
        <Ionicons name={name} size={size} color={color} />
       
      </View>
    );
  }
}

const InTransitIconWithBadge = props => {
  // You should pass down the badgeCount in some other ways like context, redux, mobx or event emitters.
  return <IconWithBadge {...props} badgeCount={1} />;
};
const AcceptedIconWithBadge = props => {
  // You should pass down the badgeCount in some other ways like context, redux, mobx or event emitters.
  return <IconWithBadge {...props} badgeCount={1} />;
};


const getTabBarIcon = (navigation, focused, tintColor) => {
  const { routeName } = navigation.state;
  let IconComponent = Ionicons;
  let iconName;
  if (routeName === 'Home') {
    iconName = `ios-home`;
    // We want to add badges to home tab icon
    IconComponent = InTransitIconWithBadge;
  }
  else if (routeName === 'Ask') {
    iconName = `ios-add`;
    IconComponent = AcceptedIconWithBadge;
  }
  else if (routeName === 'Recomendation') {
    iconName = `ios-star`;
    IconComponent = AcceptedIconWithBadge;
  }
  // You can return any component that you like here!
  return <IconComponent name={iconName} size={25} color={tintColor} />;
};

export default createAppContainer(
  createBottomTabNavigator(
    {
     
      Home: { screen: HomePage},
      Ask: { screen: NewsFeedPage },
      Recomendation: { screen: RecomendationPage },
     
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) =>
          getTabBarIcon(navigation, focused, tintColor),
      }),
      tabBarOptions: {
        activeTintColor: 'red',
        inactiveTintColor: 'white',
        activeBackgroundColor: 'black',
        inactiveBackgroundColor: 'black'
      },
    }
  )
);
