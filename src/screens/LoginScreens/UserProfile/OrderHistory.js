//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator
} from "react-native";
import Moment from 'moment';
import { Content } from "native-base";
import AccountHeader from "../../../components/AccountHeader";
import { URL, TOKEN } from "../../../components/Api";
import { TextField } from "react-native-material-textfield";
import { Icon } from "react-native-elements";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      id_s: [],
      searchResults: [],
      isLoading:true,
     
    };
    this.navigate = this.props.navigation.navigate;
  }
  componentWillMount(){
    AsyncStorage.getItem("userID").then(user_data => {
        const val = JSON.parse(user_data);
        console.log(val)
        if(val){
          this.setState({
           user_id : val.CustomerID
          });
          this.fetchSavedResults();
        }
      });
  }
  fetchSavedResults = () => {
    const {user_id} = this.state;
    console.log(user_id +"-- API--"+URL + "Order/GetHistory");
    fetch(URL + "Order/GetHistory", {
      method: "GET",
      headers: {
       AuthToken:TOKEN,
       CustomerID: user_id,
      
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.ResponseCode == 200) {
          console.log(response)
          this.setState({ searchResults: response.OrderHistoryList });
         
        } 

         else {
          Alert.alert(
            'Sorry',
            response.MessageToShow,
            [
              {text: 'OK'},
            ],
            {cancelable: true},
          );
          this.setState({ isLoading: false });
        }


      })
      .catch(error => alert("Please Check your Internet Connection"));
  }
  render() {
   
    return (
     
      <View style={{width: deviceWidth, height: deviceHeight}}>
      <AccountHeader
            back={true}
            navigation={this.props.navigation}
         />
                
               <View 
               style={{width:deviceWidth, alignItems:'center', alignContent:"center",
               marginTop:10,
               
               
               }}>
             <Text   
             style={{
                      color: "#000",
                      fontSize: 26,
                      textAlign: "left",
                      fontFamily:"neuron_bold"
                    }}>
               Order History
              </Text>
             </View>
              <Content>
             <View style={{width:deviceWidth, alignItems:'center', alignContent:"center", paddingTop:30, paddingBottom:100}}>
             {this.state.searchResults.map((item, index)=>{
                 var items = [];
                 items = item.OrderHistoryItemList;
                 console.log(items)
                        return (

                            <View key={index} style={[styles.box, styles.shadow2]}>
             <View style={{width:"100%", alignContent:"center", alignItems:"center"}}>

             <View style={{width:"60%", backgroundColor:"#45c8d0", borderRadius:5, marginTop:5}}>
                <Text style={{fontFamily:"neuron_regular", textAlign:"center"}}>Order Id: <Text style={{fontFamily:"neuron_bold"}}>{item.OrderID}</Text> </Text>
             </View>
             <View style={{width:"60%", marginTop:5}}>
                <Text style={{fontFamily:"neuron_regular", textAlign:"center"}}>Order Type: {item.IsNormalDelivery ? (<Text style={{fontFamily:"neuron_bold"}}>Normal</Text>) : item.IsPromotion ? (<Text style={{fontFamily:"neuron_bold"}}>Promotion</Text>) : item.IsPackage ? (<Text style={{fontFamily:"neuron_bold"}}>Package</Text>) : (<Text style={{fontFamily:"neuron_bold"}}>Normal</Text>)} </Text>
             </View>
             </View>
             <View style={{flexDirection:"row"}}>
               <View style={{width:"50%",padding:15}}>
               <View style={{flexDirection:"row",paddingVertical:3}}>
               <View style={{width:"50%"}}>

               <Text style={{fontSize:14, fontFamily:"neuron_bold", fontWeight:"bold", textAlign:"left"}}>Items</Text>
               </View>
               <View style={{width:"50%", paddingTop:3}}>
               <Text style={{fontSize:14, fontFamily:"neuron_bold", textAlign:"center"}}>Quantity</Text>

               </View>
               </View>
               {items.map((items, item_index)=>{
                
                return (
                    <View key={item_index} style={{flexDirection:"row",paddingVertical:3}}>
               <View style={{width:"50%"}}>

               <Text style={{fontSize:14, textAlign:"left"}}>{items.Name ? items.Name : (<Text>No. of Pieces</Text>)}</Text>
               </View>
               <View style={{width:"50%", paddingTop:3}}>
               <Text style={{fontSize:12, fontFamily:"neuron_regular", textAlign:"center", color:"#808089"}}>{items.Quantity}</Text>

               </View>
               </View>
                )
       })}
              
               </View>
               <View style={{width:"50%", padding:15}}>
               <View style={{flexDirection:"row",paddingVertical:3}}>
               <View style={{width:"50%"}}>

               <Text style={{fontSize:14, fontFamily:"neuron_bold", textAlign:"left"}}>Date Delivered</Text>
               </View>
               <View style={{width:"50%"}}>
               <Text style={{fontSize:12, fontFamily:"neuron_regular", textAlign:"center", color:"#808089"}}>{Moment(item.DeliveryDate).format('MMM DD')}</Text>

               </View>
               </View>
               <View style={{flexDirection:"row",paddingVertical:3}}>
               <View style={{width:"50%"}}>

               <Text style={{fontSize:14, fontFamily:"neuron_bold", fontWeight:"bold"}}>Bill</Text>
               </View>
               <View style={{width:"50%"}}>
               <Text style={{fontSize:12, fontFamily:"neuron_regular", textAlign:"center", color:"#808089"}}>Rs. {item.Bill}</Text>

               </View>
               </View>
              </View>
             </View>
            
         

             </View>
             
                        )
             }
             )}
            
              </View>
            

              </Content>

            {/* ............footer................. */}
           
          </View>
          

    );
  }
}
function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}
const styles = StyleSheet.create({
  shadow1: elevationShadowStyle(5),
  shadow2: elevationShadowStyle(10),
  shadow3: elevationShadowStyle(20),
  box: {
    width:"90%",
    borderRadius: 8,
    backgroundColor: 'white',
    marginTop:30
  },
  Bigbox: {
    width:"90%", 
    height:deviceHeight/3,
    borderRadius: 8,
    backgroundColor: 'white',
    marginTop:30
  },
  footer: {
    width:deviceWidth, height:60,marginTop:"auto",borderTopWidth:0.5, borderTopColor:"#808080"
  },
  shadedFooter: {
    width:deviceWidth, height:60,marginTop:"auto",
    borderRadius: 8,
    backgroundColor: 'white'
  },
  // supporting styles
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    padding: 24
  },
  centerContent: {
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  ball: {
    borderRadius: 128,
    width: 128,
    height: 128,
    backgroundColor: 'lightblue'
  }
});

export default OrderHistory;
