//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  ActivityIndicator,
  Alert,
  Picker,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import Constants from "expo-constants";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import {
  Button,
  Container,
  Icon,
  Content,
  List,
  ListItem,
  Text
} from "native-base";
import BaseHeader from "../../../components/BaseHeader";
import AccountHeader from "../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../components/Api";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class UpdateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id: "",
      first_name: "",
      last_name: "",
      username: "",
      email: "",
      password: "",
      re_password: "",
      phone: "",
      address: "",
      location: "",
      fname_err: "",
      lname_err: "",
      username_err: "",
      email_err: "",
      password_err: "",
      repass_err: "",
      phone_err: "",
      address_err: "",
      phone_err: "",
      isLoading: false,
      status: false
    };
    this.navigate = this.props.navigation.navigate;
  }
  componentWillMount () {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          customer_id: val.CustomerID,
          first_name: val.FName,
      last_name: val.LName,
      username: val.Username,
      email: val.Email,
      password: val.Password,
      phone: val.Phone,
      address: val.Address,
      location: val.Location,
        });
      }
     
    });
    this._getLocationAsync();
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        location: '',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };
  submit = async () => {
    const {
      customer_id,
      first_name,
      last_name,
      username,
      email,
      password,
      re_password,
      address,
      phone,
      location
     
    } = this.state;
    let email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (
      first_name == "" &&
      last_name == "" &&
      username == "" &&
      email == "" &&
      password == "" &&
      re_password == "" &&
      address == "" &&
      phone == ""
    ) {
      this.setState({
        fname_err: "Required",
        lname_err: "Required",
        username_err: "Required",
        email_err: "Required",
        password_err: "Required",
        repass_err: "Required",
        address_err: "Required",
        phone_err: "Required"
      });
    }

    if (first_name != "") {
      this.setState({
        fname_err: ""
      });
    } else {
      this.setState({
        fname_err: "Required"
      });
    }
    if (last_name != "") {
      this.setState({
        lname_err: ""
      });
    } else {
      this.setState({
        lname_err: "Required"
      });
    }
    if (username != "") {
      this.setState({
        username_err: ""
      });
    } else {
      this.setState({
        username_err: "Required"
      });
    }

    if (email != "") {
      if (email_reg.test(email) === false) {
        this.setState({ email_err: "Invalid Email" });
      } else {
        this.setState({ email_err: "" });
      }
    }
    
    if (email == "") {
      this.setState({ email_err: "Required" });
    }
    
    if (password == "") {
      this.setState({ password_err: "Required" });
    } else {
      this.setState({
        password_err: ""
      });
    }
    if (re_password == "") {
      this.setState({ repass_err: "Required" });
    } else {
      this.setState({
        repass_err: ""
      });
    }
    if (re_password != password) {
      this.setState({ repass_err: "Password doesn't match" });
    } else {
      this.setState({
        repass_err: ""
      });
    }
    if (address == "") {
      this.setState({ address_err: "Required" });
    } else {
      this.setState({
        address_err: ""
      });
    }
   
    if (phone == "") {
      this.setState({ phone_err: "Required" });
    } else {
      this.setState({
        phone_err: ""
      });
    }
    if (
      this.state.first_name != "" &&
      this.state.last_name != "" &&
      this.state.username != "" &&
      this.state.email != "" &&
      email_reg.test(email) === true &&
      this.state.password != "" &&
      this.state.re_password != "" &&
      this.state.re_password == this.state.password &&
      this.state.address != "" &&
      this.state.phone != ""
    ) {
      this.setState({ isLoading: true });

      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        console.log("not granted")
      }
      let token = await Notifications.getExpoPushTokenAsync();
      let latitude = ""; 
      let longitude = ""; 
      if(location){
latitude = location.coords.latitude;
longitude = location.coords.longitude;
      }
      fetch(URL + "Customer/UpdateByID", {
        method: "PUT",
        headers: {
          AuthToken: TOKEN,
          CustomerID: customer_id,
          FName: first_name,
          LName: last_name,
          Email: email,
          Username: username,
          Password: password,
          Phone: phone,
          Address: address,
          Lat: latitude,
          Long: longitude,
          DeviceID : token
        }
      })
        .then(res => res.json())
        .then(response => {
          if (response.ResponseCode == 200) {
            Alert.alert("Success", "User Updated", [{ text: "OK" }], {
              cancelable: true
            });
            this.setState({ isLoading: false });
            this.props.navigation.push("LoginForm");
          } else {
            Alert.alert("Sorry", response.MsgToShow, [{ text: "OK" }], {
              cancelable: true
            });
            this.setState({ isLoading: false });
          }
        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }
  };

  render() {
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
         <AccountHeader navigation={this.props.navigation} back={true} />

          <View
            style={{
              alignItems: "center",
              paddingTop: 20
            }}
          >
            {Platform.OS == "ios" ? (
              <KeyboardAvoidingView behavior="padding" enabled>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ alignItems: "center", alignContent: "center" }}>
                  <Text
                   style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}
                  >
                    Update Account
                  </Text>
                </View>
                <View
                  style={{
                    width: (deviceWidth * 2) / 2.5,
                    paddingBottom: deviceHeight / 2
                  }}
                >
                  <TextField
                    autoCapitalize="none"
                    error={this.state.fname_err}
                    onChangeText={first_name => this.setState({ first_name })}
                    autoCorrect={false}
                    label="First Name"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    autoCapitalize="none"
                    error={this.state.lname_err}
                    onChangeText={last_name => this.setState({ last_name })}
                    autoCorrect={false}
                    label="Last Name"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />

                  <TextField
                    autoCapitalize="none"
                    error={this.state.username_err}
                    //labelHeight={heightPercentageToDP(1)}
                    onChangeText={username => this.setState({ username })}
                    autoCorrect={false}
                    label="Username"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    keyboardType="email-address"
                    labelPadding={1}
                    error={this.state.email_err}
                    autoCapitalize="none"
                    //labelHeight={heightPercentageToDP(1)}
                    onChangeText={email => this.setState({ email })}
                    autoCorrect={false}
                    label="EMAIL"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    secureTextEntry={true}
                    labelPadding={1}
                    error={this.state.password_err}
                    autoCapitalize="none"
                    onChangeText={password => this.setState({ password })}
                    autoCorrect={false}
                    label="Password"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    secureTextEntry={true}
                    labelPadding={1}
                    error={this.state.repass_err}
                    autoCapitalize="none"
                    onChangeText={re_password => this.setState({ re_password })}
                    autoCorrect={false}
                    label="Confirm Password"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    labelPadding={1}
                    error={this.state.phone_err}
                    keyboardType={'phone-pad'}
                    autoCapitalize="none"
                    onChangeText={phone => this.setState({ phone })}
                    autoCorrect={false}
                    label="Phone"
                    maxLength={15}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    labelPadding={1}
                    autoCapitalize="none"
                    error={this.state.address_err}
                    onChangeText={address => this.setState({ address })}
                    autoCorrect={false}
                    label="Address"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      alignContent: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Button
                        onPress={this.submit}
                      bordered
                      heightPercentageToDP={50}
                      style={{
                        width: (deviceWidth * 2) / 3,
                        height: 50,
                        alignItems: "center",
                        justifyContent: "center",
                        marginTop: 15,
                        borderColor: "#45c8d0",
                        backgroundColor: "#45c8d0",
                      }}
                    >
                    {this.state.isLoading ? (
<ActivityIndicator size={'small'}/>
                    ):(
                      <Text
                        style={{
                          textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                        }}
                      >
                        Update
                      </Text>
                    )}
                      
                    </Button>
                  </View>
                 
                </View>
              </ScrollView>
            
               </KeyboardAvoidingView>
            ) : (
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ alignItems: "center", alignContent: "center" }}>
                  <Text
                   style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}
                  >
                    Update Account
                  </Text>
                </View>
                <View
                  style={{
                    width: (deviceWidth * 2) / 2.5,
                    paddingBottom: deviceHeight / 2
                  }}
                >
                  <TextField
                    autoCapitalize="none"
                    error={this.state.fname_err}
                    onChangeText={first_name => this.setState({ first_name })}
                    value={this.state.first_name}
                    autoCorrect={false}
                    label="First Name"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    autoCapitalize="none"
                    error={this.state.lname_err}
                    onChangeText={last_name => this.setState({ last_name })}
                    value={this.state.last_name}
                    autoCorrect={false}
                    label="Last Name"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />

                  <TextField
                    autoCapitalize="none"
                    error={this.state.username_err}
                    value={this.state.username}
                    onChangeText={username => this.setState({ username })}
                    autoCorrect={false}
                    label="Username"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    keyboardType="email-address"
                    labelPadding={1}
                    error={this.state.email_err}
                    autoCapitalize="none"
                    value={this.state.email}
                    onChangeText={email => this.setState({ email })}
                    autoCorrect={false}
                    label="EMAIL"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    secureTextEntry={true}
                    labelPadding={1}
                    error={this.state.password_err}
                    value={this.state.password}
                    autoCapitalize="none"
                    onChangeText={password => this.setState({ password })}
                    autoCorrect={false}
                    label="Password"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    secureTextEntry={true}
                    labelPadding={1}
                    error={this.state.repass_err}
                    autoCapitalize="none"
                    onChangeText={re_password => this.setState({ re_password })}
                    autoCorrect={false}
                    label="Confirm Password"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    labelPadding={1}
                    error={this.state.phone_err}
                    keyboardType={'phone-pad'}
                    autoCapitalize="none"
                    value={this.state.phone}
                    onChangeText={phone => this.setState({ phone })}
                    autoCorrect={false}
                    label="Phone"
                    maxLength={15}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <TextField
                    labelPadding={1}
                    autoCapitalize="none"
                    error={this.state.address_err}
                    value={this.state.address}
                    onChangeText={address => this.setState({ address })}
                    autoCorrect={false}
                    label="Address"
                    maxLength={40}
                    tintColor={"#000"}
                    baseColor="#000"
                    textColor="#000"
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      alignContent: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Button
                        onPress={this.submit}
                      bordered
                      heightPercentageToDP={50}
                      style={{
                        width: (deviceWidth * 2) / 3,
                        height: 50,
                        alignItems: "center",
                        justifyContent: "center",
                        marginTop: 15,
                        borderColor: "#45c8d0",
                        backgroundColor: "#45c8d0",
                      }}
                    >
                    {this.state.isLoading ? (
<ActivityIndicator size={'small'}/>
                    ):(
                      <Text
                        style={{
                          textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                        }}
                      >
                        Update
                      </Text>
                    )}
                      
                    </Button>
                  </View>
                 
                </View>
              </ScrollView>
            )}

            {/* ............Button................. */}
          </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default UpdateAccount;
