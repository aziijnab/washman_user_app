//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Container } from "native-base";
import BaseHeader from "../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      username_err: "",
      isLoading: false,
    };
  }
  submit = () => {
    const { username } = this.state;
    if (username == "") {
      this.setState({ username_err: "Required" });
    }
    else{
      this.setState({ username_err: "" });
    }
 
    if(username != ""){
      this.props.navigation.navigate("RecoverPassword", {
        username : username
      })
      this.setState({ isLoading: true });
      fetch(URL + "Customer/ForgotPassword", {
        method: "GET",
        headers: {
         AuthToken:TOKEN,
         Username: username,
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.ResponseCode == 200) {
            this.setState({ isLoading: false });
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
          } 
           else {

            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            
            this.setState({ isLoading: false });
          }


        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }
  };
  render() {
    const { navigation } = this.props.navigation;
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
     <BaseHeader back={true} navigation={this.props.navigation}/>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:50}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_regular",textAlign:'center' }}>
               "Enter your username"
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100,
                paddingTop:'10%'
              }}
            >
              <TextField
                autoCapitalize="none"
                error={this.state.username_err}
                onChangeText={username => this.setState({ username })}
                autoCorrect={false}
                label="Username"
                maxLength={20}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
             
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: deviceHeight/5,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                   onPress={this.submit}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: deviceHeight/5,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    SEND
                  </Text>
                </Button>
              )}
              </View>
              </View>

            {/* ............Button................. */}
          </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default ForgotPassword;
