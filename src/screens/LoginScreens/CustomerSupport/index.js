//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator,
  
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Container, Form, Textarea, Toast, Content } from "native-base";
import AccountHeader from "../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class CustomerSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      subject: "",
      message: "",
      email_err: "",
      subject_err: "",
      message_err: "",
      isLoading: false,
    };
    this.data = this.props.navigation.state.params;
  }
  submit = () => {
    const { email , subject, message} = this.state;
    
    if (email == "") {
      this.setState({ email_err: "Required" });
    }
    else{
      this.setState({ email_err: "" });
    }
    if (subject == "") {
      this.setState({ subject_err: "Required" });
    }
    else{
      this.setState({ subject_err: "" });
    }
    if (message == "") {
      this.setState({ message_err: "Required" });
    }
    else{
      this.setState({ message_err: "" });
    }
 
    if(email != "" && subject != "" && message !=""){
      this.setState({ isLoading: true });
      fetch(URL + "ContactUs/Add", {
        method: "POST",
        headers: {
         AuthToken:TOKEN,
         email: email,
         Subject: subject,
         Details: message,
        }
      })
        .then(res => res.json())
        .then(async response => {
          if (response.ResponseCode == 200) {
            Alert.alert(
              'Success',
              "Message has been sent",
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }


        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }
  };
  render() {
    const { navigation } = this.props.navigation;
    return (

      <View style={{ height: deviceHeight, width: deviceWidth }}>
       <AccountHeader navigation={this.props.navigation} back={true} />
<Content>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:50}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_regular",textAlign:'center' }}>
               "CUSTOMER SUPPORT"
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100,
                paddingTop:'10%'
              }}
            >
              <TextField
                autoCapitalize="none"
                error={this.state.email_err}
                onChangeText={email => this.setState({ email })}
                autoCorrect={false}
                label="Email"
                maxLength={20}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
              <TextField
                autoCapitalize="none"
                error={this.state.subject_err}
                onChangeText={subject => this.setState({ subject })}
                autoCorrect={false}
                label="Subject"
                maxLength={20}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
              <Form>
            <Textarea 
             onChangeText={message => this.setState({ message })}
              rowSpan={5} bordered placeholder="Message" />
          </Form>
             
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center"
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 20,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                   onPress={this.submit}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    marginTop: 20,
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    SEND
                  </Text>
                </Button>
              )}
              </View>
              </View>

            {/* ............Button................. */}
          </View>
       
          </Content>
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default CustomerSupport;
