//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import Constants from "expo-constants";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class skip1 extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ImageBackground
        source={require("../../../assets/skip1.png")}
        resizeMode={"cover"}
        style={{
          height: deviceHeight,
          width: deviceWidth,
        }}
      >
        <View style={{ marginTop: "auto", marginBottom: 80, marginLeft: 80 }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("skip2")}
          >
            <Text
              style={{
                color: "#fff",
                fontSize: 30,
                textAlign: "left",
                fontFamily: "neuron_regular"
              }}
            >
              Skip
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default skip1;
