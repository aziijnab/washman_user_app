import React from "react";
import {
  createStackNavigator,
  createAppContainer,
  StackNavigator
} from "react-navigation";
import UpdateAccount from "./screens/LoginScreens/UserProfile/UpdateAccount";
import LoginForm from "./screens/LoginScreens/LoginForm";
import skip1 from "./screens/SkipScreens/skip1";
import skip2 from "./screens/SkipScreens/skip2";
import skip3 from "./screens/SkipScreens/skip3";
import skip4 from "./screens/SkipScreens/skip4";
import RegisterScreens from "./screens/RegisterScreens";
import HomeTab from "./screens/LoginScreens/UserProfile/home_tabs/HomeTab";
import ActionPage from "./screens/LoginScreens/UserProfile/home_tabs/ActionPage";
import OrderFromPackage from "./screens/LoginScreens/UserProfile/home_tabs/OrderFromPackage";
import Quantity from "./screens/LoginScreens/UserProfile/home_tabs/Quantity";
import Order from "./screens/LoginScreens/UserProfile/home_tabs/Order";
import SubcriptionOrder from "./screens/LoginScreens/UserProfile/home_tabs/SubcriptionOrder";
import PromotionOrder from "./screens/LoginScreens/UserProfile/home_tabs/PromotionOrder";
import CategoryDetails from "./screens/LoginScreens/UserProfile/home_tabs/CategoryDetails";
import Settings from "./screens/LoginScreens/UserProfile/Settings";
import OrderHistory from "./screens/LoginScreens/UserProfile/OrderHistory";
import DeactivateAccount from "./screens/LoginScreens/UserProfile/DeactivateAccount";
import ForgotPassword from "./screens/LoginScreens/ForgotPassword";
import RecoverPassword from "./screens/LoginScreens/RecoverPassword";
import CustomerSupport from "./screens/LoginScreens/CustomerSupport";
import AvailablePromotions from "./screens/LoginScreens/UserProfile/home_tabs/AvailablePromotions";
import PackageSubscription from "./screens/LoginScreens/UserProfile/home_tabs/PackageSubscription";
import CurrentPackage from "./screens/LoginScreens/UserProfile/home_tabs/CurrentPackage";
import TermsandConditions from "./screens/LoginScreens/UserProfile/home_tabs/TermsandConditions";



const TopLevelNavigator = createStackNavigator(
  {
    skip1: { screen: skip1 },
    skip2: { screen: skip2 },
    skip3: { screen: skip3 },
    skip4: { screen: skip4 },
    LoginForm: { screen: LoginForm },
    RegisterScreens: { screen: RegisterScreens },
    ActionPage: { screen: ActionPage },
    Settings: { screen: Settings },
    OrderHistory: { screen: OrderHistory },
    HomeTab: { screen: HomeTab },
    CategoryDetails: { screen: CategoryDetails },
    Quantity: { screen: Quantity },
    SubcriptionOrder: { screen: SubcriptionOrder},
    OrderFromPackage: { screen: OrderFromPackage},
    Order: { screen: Order},
    UpdateAccount: { screen: UpdateAccount },
    DeactivateAccount: { screen: DeactivateAccount },
    ForgotPassword: { screen: ForgotPassword },
    RecoverPassword: { screen: RecoverPassword },
    CustomerSupport: { screen: CustomerSupport },
    AvailablePromotions: {screen: AvailablePromotions},
    PackageSubscription: { screen: PackageSubscription },
    CurrentPackage: { screen: CurrentPackage},
    TermsandConditions: { screen: TermsandConditions},
    PromotionOrder: { screen: PromotionOrder},
  },
  {
    headerMode: "none"
  }
);

const TopLevel = createAppContainer(TopLevelNavigator);
export default TopLevel;
