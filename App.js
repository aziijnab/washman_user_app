//import React from 'react';
import React, { Component } from "react";
import TopLevelNavigator from './src/AppNavigator'
import TopLevelLoginNavigator from './src/LoginNavigator'
import { StyleSheet, Text, View, AsyncStorage, AppRegistry } from 'react-native';
import { AppLoading } from "expo";
import * as Font from 'expo-font'
import Firebase from 'firebase';
let config = {
  apiKey: 'AIzaSyCOseuJoGBbzlW04k27qztxL7gqzF3P8pM',
  authDomain: 'washman-8a383.firebaseapp.com',
  databaseURL: 'https://washman-8a383.firebaseio.com/',
  projectId: 'washman-8a383',
  storageBucket: 'washman-8a383.appspot.com',
  messagingSenderId: '985379261250'
};
Firebase.initializeApp(config);
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn : false
    }
    this.state = { loading: true };
  }
  async componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({
          loggedIn: true,
        });
      }
    });
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      neuron_regular: require("./assets/fonts/Neuron_Regular.otf"),
      neuron_bold: require("./assets/fonts/Neuron_Bold.otf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return (
        // <Root>
          <AppLoading />
        // {/* </Root> */}
      );
    }
   
    if (this.state.loggedIn){
      return (
        <TopLevelLoginNavigator />
      );
    }else{
      return (
        <TopLevelNavigator />
      );
    }
    
  }
}
AppRegistry.registerComponent('Washman', () => App);
